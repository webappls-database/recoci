SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS access (
id int(11) NOT NULL,
  id_user int(11) NOT NULL,
  section varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  flag_out int(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  notes text COLLATE utf8_unicode_ci,
  id_modifier int(11) NOT NULL DEFAULT '0',
  deleted int(1) NOT NULL DEFAULT '0',
  modify_time datetime DEFAULT NULL,
  modify_ip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS city (
id int(11) NOT NULL,
  city_code_istat mediumint(6) unsigned NOT NULL DEFAULT '0',
  city_code_istat_province smallint(3) unsigned NOT NULL DEFAULT '0',
  city_code_istat_city smallint(3) unsigned NOT NULL DEFAULT '0',
  city_code_land char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  city_code_postal mediumint(5) unsigned NOT NULL DEFAULT '0',
  city_name varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  city_province_code enum('AG','AL','AN','AO','AP','AQ','AR','AT','AV','BA','BG','BI','BL','BN','BO','BR','BS','BT','BZ','CA','CB','CE','CH','CI','CL','CN','CO','CR','CS','CT','CZ','EE','EN','FC','FE','FG','FI','FM','FR','GE','GO','GR','IM','IS','KR','LC','LE','LI','LO','LT','LU','MB','MC','ME','MI','MN','MO','MS','MT','NA','NO','NU','OG','OR','OT','PA','PC','PD','PE','PG','PI','PN','PO','PR','PT','PU','PV','PZ','RA','RC','RE','RG','RI','RM','RN','RO','SA','SI','SO','SP','SR','SS','SV','TA','TE','TN','TO','TP','TR','TS','TV','UD','Unknown','VA','VB','VC','VE','VI','VR','VS','VT','VV') NOT NULL DEFAULT 'Unknown',
  city_province_name varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  city_region enum('Abruzzo','Africa','America Centrale','America Meridionale','America Settentrionale','Asia','Basilicata','Calabria','Campania','Emilia-Romagna','Europa','Friuli-Venezia Giulia','Lazio','Liguria','Lombardia','Marche','Molise','Oceania','Piemonte','Puglia','Sardegna','Sicilia','Terre Polari Antartiche','Terre Polari Artiche','Toscana','Trentino-Alto Adige','Umbria','Unknown','Valle d''Aosta/Vallée d''Aoste','Veneto') NOT NULL DEFAULT 'Unknown',
  city_telephone_prefix varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  city_website varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  city_deleted enum('Yes','No') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  city_moved enum('Yes','No') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  city_moved_code_istat mediumint(8) unsigned NOT NULL DEFAULT '0',
  city_notes text NOT NULL,
  notes text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  id_modifier int(11) NOT NULL,
  deleted int(1) DEFAULT '0',
  modify_time datetime DEFAULT NULL,
  modify_ip varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS station (
id int(11) NOT NULL,
  id_city int(11) NOT NULL,
  email varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  image varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  phone varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  creation_time datetime NOT NULL,
  notes text COLLATE utf8_unicode_ci,
  id_modifier int(11) NOT NULL,
  deleted int(1) DEFAULT '0',
  modify_time datetime DEFAULT NULL,
  modify_ip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS subscriber (
id int(11) NOT NULL,
  id_station int(11) NOT NULL,
  id_user int(11) NOT NULL COMMENT 'utente che ha effettuato la registrazione',
  email varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  surname varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  image varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  doc varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  phone varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  creation_time datetime NOT NULL,
  birth_date date DEFAULT NULL,
  id_city_birth int(11) DEFAULT NULL,
  address varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  id_city_residential int(11) DEFAULT NULL,
  sex enum('F','M') COLLATE utf8_unicode_ci DEFAULT NULL,
  tax_code varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  forced int(1) NOT NULL DEFAULT '0',
  notes text COLLATE utf8_unicode_ci,
  id_modifier int(11) NOT NULL,
  deleted int(1) DEFAULT '0',
  modify_time datetime DEFAULT NULL,
  modify_ip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `user` (
id int(11) NOT NULL,
  id_station int(11) NOT NULL,
  supervisor int(1) NOT NULL DEFAULT '0',
  pwd varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  surname varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  image varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  phone varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  creation_time datetime NOT NULL,
  pwd_modify_time datetime NOT NULL,
  notes text COLLATE utf8_unicode_ci,
  id_modifier int(11) NOT NULL,
  deleted int(1) DEFAULT '0',
  modify_time datetime DEFAULT NULL,
  modify_ip varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE access
 ADD PRIMARY KEY (id), ADD KEY id_user (id_user), ADD KEY `time` (`time`), ADD KEY section (section);

ALTER TABLE city
 ADD PRIMARY KEY (id), ADD KEY city_code_istat (city_code_istat);

ALTER TABLE station
 ADD PRIMARY KEY (id), ADD KEY city_id (id_city);

ALTER TABLE subscriber
 ADD PRIMARY KEY (id), ADD KEY id_station (id_station), ADD KEY id_user (id_user), ADD KEY tax_code (tax_code);

ALTER TABLE user
 ADD PRIMARY KEY (id), ADD KEY id_station (id_station);


ALTER TABLE access
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE city
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE station
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE subscriber
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE user
MODIFY id int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# README #

Registro Consultazioni Civiche

programma web realizzato da [WebAppls](http://www.webappls.com) e [Database Informatica](http://www.database.it) utilizzato per la registrazione degli iscritti alla consultazione civica organizzata da [Coalizione Civica](http://www.coalizionecivica.it/) il 28/02/2016 a Bologna.

Basato sulle librerie [walibs3](https://github.com/fabriziomonti/walibs3).
//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/**
* classe wadata_inline: controllo per l'input di una data
* 
* @class wadata_inline
* @extends wacontrollo
*/
var wadata_inline = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: wacontrollo,

	//-------------------------------------------------------------------------
	// proprieta'
	tipo		: 'data_inline',
	myonblur	: function onblur(event) {this.form.wamodulo.controlli[this.name].allaPerditaFuoco(event);},
	onblur		: false,
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function(modulo, nome, valore, visibile, solaLettura, obbligatorio) 
		{
		// definizione iniziale delle proprieta'
		this.parent(modulo, nome, valore, visibile, solaLettura, obbligatorio);
		this.obj.onblur = this.myonblur;
		},
		
	//-------------------------------------------------------------------------
	allaPerditaFuoco: function(event) 
		{
		// se hanno scritto solo i digit, aggiungiamo i separatori
		this.formatta();
		this.eventoApplicazione(event, "onblur");
		},
		
	//-------------------------------------------------------------------------
	formatta: function(event) 
		{
		// se hanno scritto solo i digit, aggiungiamo i separatori
		if (this.obj.value.length == 8)
			{
			var g = this.obj.value.substring(0, 2);
			var m = this.obj.value.substring(2, 4);
			var y = this.obj.value.substring(4);
			this.obj.value = g + "/" + m + "/" + y;
			}
		},
		
	//-------------------------------------------------------------------------
	// restituisce il valore logico del controllo
	dammiValore: function() 
		{
		if (this.obj.value == '')
			return false;
		if (!this.verificaForma())
			return false;
		var retval = new Date(this.obj.value.substring(6), 
								this.obj.value.substring(3, 5) - 1, 
								this.obj.value.substring(0, 2));
		
		return retval;
		},
		
	//-------------------------------------------------------------------------
	// inserisce un valore logico nel controllo
	mettiValore: function(valore) 
		{
		if (!valore || valore == 0 || valore == '')
			this.obj.value = '';
		else
			{
			this.obj.value = valore.getDate() + "/" +
							(valore.getMonth() + 1) + "/" + 
							valore.getFullYear();
			}
		},
		
	//-------------------------------------------------------------------------
	verificaForma: function() 
		{
		if (this.obj.value == '')
			return true;
			
		this.formatta();
		var g = this.obj.value.substring(0, 2);
		var m = this.obj.value.substring(3, 5);
		var y = this.obj.value.substring(6);
		if (y.length < 4)
			return false;
		
		var testDate = new Date(y, m - 1, g);
		
		return g == testDate.getDate() &&
					m == (testDate.getMonth() + 1) &&
					y == testDate.getFullYear();
		
		}
		
	
	
	}
);

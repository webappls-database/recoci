<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- ********************************************************************** -->
<!-- template data                                                          -->
<!-- ********************************************************************** -->
<xsl:template match="data_inline" name="data_inline">
	
	<xsl:variable name="val">
		<xsl:if test="valore != ''">
			<xsl:value-of select="substring(valore, 9, 2)" />
			<xsl:text>/</xsl:text>
			<xsl:value-of select="substring(valore, 6, 2)" />
			<xsl:text>/</xsl:text>
			<xsl:value-of select="substring(valore, 1, 4)" />
		</xsl:if>
	</xsl:variable>

	<xsl:call-template name="intestazione_controllo"/>
	<div class="controllo">
		<input type='text' name='{@id}' id='{@id}' value='{$val}' size='10' maxlength='10'>
			<xsl:call-template name="dammiattributicontrollo"/>
		</input>

	</div>
			
</xsl:template>

<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
<xsl:template match="data_inline.input">
	<xsl:element name="{@id}">
		<xsl:variable name="id" select="@id" />
		<xsl:choose>
			<xsl:when test="/wamodulo.input/post/item[@id=$id]">
				<xsl:variable name="giorno" select="substring(/wamodulo.input/post/item[@id=$id], 1, 2)" />
				<xsl:variable name="mese" select="substring(/wamodulo.input/post/item[@id=$id], 4, 2)" />
				<xsl:variable name="anno" select="substring(/wamodulo.input/post/item[@id=$id], 7, 4)" />
				<xsl:value-of select="concat($anno, '-', $mese, '-', $giorno)" />
			</xsl:when>
			<xsl:otherwise>__wamodulo_valore_non_ritornato__</xsl:otherwise>
		</xsl:choose>
	</xsl:element>
</xsl:template>


</xsl:stylesheet>
<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
if (!defined('__CONFIG_VARS'))
{
	define('__CONFIG_VARS',1);
	
	$__DIR__ = __DIR__;

	// file contenente i veri valori di configurazione, che non devono finire
	// su git, mentre il presente file rimane per documentazione
	@include "$__DIR__/myconfig.inc.php";
	
	// file contenente i parametri della versione (che cambiano e devono essere 
	// inviati al server, a differenza di questi)
	include "$__DIR__/versionconfig.inc.php";
	
	define('APPL_DOMAIN', 						$_SERVER['HTTP_HOST']);
	define('APPL_DIRECTORY', 					'');
	define('APPL_TMP_DIRECTORY', 				"$__DIR__/web_files/tmp");
	define('APPL_DOC_DIRECTORY', 				"$__DIR__/web_files");
	define('APPL_WEB_DOC_DIRECTORY', 			APPL_DIRECTORY .'/web_files');		// attenzione: non usare!
	define('APPL_NAME', 						'recoci');
	define('APPL_TITLE', 						'Registro consultazioni civiche');
	define('APPL_SMTP_SERVER', 					'');
	define('APPL_SMTP_USER', 					'');
	define('APPL_SMTP_PWD', 					'');
	define('APPL_SMTP_SECURE',					'');
	define('APPL_SMTP_PORT',					'');
	define('APPL_SUPPORT_ADDR', 				'support@webappls.com');
	define('APPL_INFO_ADDR', 					'info@webappls.com');
	define('APPL_SUPPORT_TEL', 					'051 232260');
	
	define("WAMODULO_EXTENSIONS_DIR", 			"$__DIR__/wamodulo_ext");	// directory estensioni classe modulo
	define("APPL_PWD_PWD", 						"");	// passphrase encryption password
	define("APPL_NOME_FILE_LOG_DB", 			"$__DIR__/web_files/logdb/logdb.csv");	// nome file di logging scritture su db
	
	// 	// define per la genearazione della documentazione online
	// define("APPL_GENERA_WADOC",				true);	
	// define per la gestione della tabella help
	// define("APPL_VEDI_MENU_HELP", 			true);	
	// define per la gestione del debug
	define("APPL_DEBUG",			 			true);	
	
	set_include_path(get_include_path() . ":$__DIR__/../../third_party_libs:$__DIR__/../../third_party_libs/moduli_pear:$__DIR__/lib");
	
	
} //  if (!defined('__CONFIG_VARS'))

<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//error_reporting(E_ALL);


//****************************************************************************************
include_once (__DIR__ . "/defines.inc.php");
include_once (__DIR__ . "/config.inc.php");
include_once (__DIR__ . "/walibs3/waapplicazione/waapplicazione.inc.php");


//****************************************************************************************
class recoci extends waApplicazione
	{
	var $directoryDoc;
	var $webDirectoryDoc;
	var $datiSessione;
	var $fileConfigDB = '';
	
	// passphrase encryption password
	var $pwdPwd = '';
	
	// dati di editing da appiccicare a tutte le modifiche sulle tabelle del db
	var $moduloModId = "modify_time";
	var $moduloModIp = "modify_ip";
	
	/**
	* contiene i dati dell'operatore loggato alla sessione
	*/
	var $utente = array();

	public static $istanzaApplicazione;
	
	//****************************************************************************************
	/**
	* costruttore
	*
	*/
	function __construct($usaSessione = true)
		{
		$this->usaSessione = $usaSessione;
		$this->dominio = APPL_DOMAIN;
		$this->httpwd = APPL_DIRECTORY;
		$this->directoryTmp = APPL_TMP_DIRECTORY;
		$this->nome = APPL_NAME;
		$this->titolo = APPL_TITLE;
		$this->versione = APPL_REL;
		$this->dataVersione = APPL_REL_DATE;
		$this->serverSmtp = APPL_SMTP_SERVER;
		$this->utenteSmtp = APPL_SMTP_USER;
		$this->passwordSmtp = APPL_SMTP_PWD;
		$this->sicurezzaSmtp = APPL_SMTP_SECURE;
		$this->portaSmtp = APPL_SMTP_PORT;
		$this->emailSupporto = APPL_SUPPORT_ADDR;
		$this->emailInfo = APPL_INFO_ADDR;
		$this->telSupporto = APPL_SUPPORT_TEL;
		
		$this->directoryDoc = APPL_DOC_DIRECTORY;
		$this->webDirectoryDoc = APPL_WEB_DOC_DIRECTORY;
		$this->pwdPwd = APPL_PWD_PWD;
		
		$this->fileConfigDB = __DIR__ . "/dbconfig.inc.php";
		$subdir = $this->siglaSezione ? "/$this->siglaSezione" : '';
		$nome_xslt = $this->siglaSezione ? $this->siglaSezione : $this->nome;
		$this->xslt = __DIR__ . "/ui/xslt$subdir/$nome_xslt.xsl";

		// differenziamo il cookie di sessione tra le diverse versioni
		session_set_cookie_params(ini_get('session.cookie_lifetime'), "$this->httpwd/$this->siglaSezione");
		self::$istanzaApplicazione = $this;
		
		$this->inizializza();
        
		}
	
	//*****************************************************************************
	/**
	* a seconda dello stato della form, chiama l'opportuno metodo
	*
	* @return waConnessiondDB
	*/
	function dammiConnessioneDB($fileConfigurazioneDB = '')
	    {
		if (empty($fileConfigurazioneDB))
			$fileConfigurazioneDB = $this->fileConfigDB;
	    return parent::dammiConnessioneDB($fileConfigurazioneDB);
		}
	    
	//*****************************************************************************
	function record2Array($record)
		{
		$retval = array();
		$rs = $record->righeDB;
		for ($i = 0; $i < $rs->nrCampi(); $i++)
			{
			if ($rs->tipoCampo($i) <> WADB_CONTENITORE)
				$retval[strtolower($rs->nomeCampo($i))] = $record->valore($i);
			}
		
		return $retval;
		}
		
	//***************************************************************************
	/**
	* -
	* @return waModulo
	*/
	function dammiModulo($paginaDestinazione = null)
		{
		$modulo = new waModulo($paginaDestinazione, $this);
		$subdir = $this->siglaSezione ? "/$this->siglaSezione" : '';
		$modulo->xslt = __DIR__ . "/ui/xslt$subdir/wamodulo.xsl";
		$modulo->larghezza = 800;
		$modulo->nomeCampoModId = $this->moduloModId;
		
		return $modulo;
		}
		
	//***************************************************************************
	function decryptPassword($pwd)
		{
		// in mysql: aes_decrypt(unhex(campo, chiave)
		$dec = @mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->pwdPwd, pack("H*" , $pwd), MCRYPT_MODE_ECB);
		$toret = rtrim($dec, ((ord(substr($dec, strlen($dec) - 1, 1)) >= 0 and ord(substr($dec, strlen($dec) - 1, 1 ) ) <= 16 ) ? chr(ord(substr($dec, strlen($dec ) - 1, 1))): null) );			
		
		return $toret;
		}

	//***************************************************************************
	function encryptPassword($pwd)
		{
		// in mysql: hex(aes_encrypt(campo, chiave)
		// MySQL Padding
		$pad_len = 16 - (strlen($pwd) % 16);
		$pwd = str_pad($pwd, (16 * (floor(strlen($pwd) / 16) + 1)), chr($pad_len));
		 
		mt_srand();
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
		mcrypt_generic_init($td, $this->pwdPwd, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
		$encrypted = mcrypt_generic($td, $pwd);
		mcrypt_generic_deinit($td);
		
		$toret = '';
		for ($i = 0; $i < strlen($encrypted); $i += 1)
			$toret .= str_pad(dechex(ord(substr($encrypted, $i, 1))), 2, '0', STR_PAD_LEFT);
			
		return strtoupper($toret);
		}
		
	//***************************************************************************
	// ritorna una chiave personale da scrivere sul record
	//***************************************************************************
	function getPassword()
		{
		$mt = microtime();
		$elems = explode(" ", microtime());
		$pwd = chr((substr($elems[1], -1) + ord('A'))) . substr($elems[0], 2, 4) . substr($elems[1], -3);
		$pwd = substr($pwd, 0, -1) . chr(substr($pwd, -1) + ord('l'));
		return $pwd;
		}
		
	//***************************************************************************
	// ritorna un token
	//***************************************************************************
	function getToken()
		{
		return substr($this->encryptPassword($this->encryptPassword($this->getPassword())), 0, 64);
		}
		
	//*****************************************************************************
	function setEditorData(waRecord $riga)
		{
		/**
		 * @todo da sistemare: in operations $this->utente è un oggetto
		 */
		$riga->inserisciValore("id_modifier", is_object($this->utente) ? $this->utente->id : $this->utente['id']);
		$riga->inserisciValore($this->moduloModId, time());
		$riga->inserisciValore($this->moduloModIp, $_SERVER['REMOTE_ADDR']);
		}

	//**************************************************************************
	// l'eliminazione standard di un record è logica, non fisica
	function eliminaRecord(waRecord $riga, $fine_script = true)
		{
		if (!$riga)
			return;
		
		$riga->inserisciValore("deleted", 1);
		$this->setEditorData($riga);
		$this->salvaRigheDB($riga->righeDB);
		if ($fine_script)
			$this->ritorna();
		}

	//**************************************************************************
	// verifica che un modulo soddisfi i requisiti di obbligatorietà, 
	// altrimenti interrompe l'operazione di salvataggio con un messaggio
	function verificaObbligo(waModulo $modulo)
		{
		if (!$modulo->verificaObbligo())
			$this->mostraErroreObbligatorieta ();
		}

	//***************************************************************************
	/**
	* manda in output la pagina
	* 
	* manda in output la pagina, compresi gli elementi aggiunti alla pagina 
	* tramite il metodo {@link aggiungiElemento}, utilizzando il foglio di 
	* stile indicato nella proprietà {@link xslt}.
	* @param boolean $bufferizza se false, allora viene immediatamente effettuato 
	* l'output della pagina; altrimenti la funzione ritorna il buffer di output 
	 * 
	 * non si capisce perchè in php 5.2 (o forse è la versione di libxml)
	 * aggiunge in alcuni casi un cdata al tag <script type="text/javascript"...
	 * 
	 * per questo motivo facciamo l'overloading: per togliere sta roba dalle 
	 * palle
	* @return void|string
	*/
	function mostra($bufferizza = false)
		{
		if (defined("APPL_DEBUG") && $_GET['xml'] == 1)
			{
			$this->mostraXML(); 
			exit();
			}
		
		$this->costruisciXML();
		$html = $this->trasforma();
		
		if (stripos($html, "<html") !== false)
			// effettuiamo la correzione "strict" solo se stiamo
			// effettivamente mandando in output un'intera pagina HTML
			// (se non è una pagina intera - ad esempio il contenuto di un div -
			// o se l'output non è html, allora non si fa nessun controllo 
			// strict)
			$html = $this->correggiHTML($html);
		
		if ($bufferizza)
			return $html;
			
		header("Content-Type: text/html; charset=utf-8");			
		echo $html;

		}
		
	//*****************************************************************************
	function verificaViolazioneLock(waModulo $modulo)
		{
		if ($modulo->record->valore($this->moduloModId) != $modulo->dammiModId())
			$this->mostraMessaggio("Errore violazione lock",
							"Errore di violazione del lock del record: il record e'" .
							" stato modificato da un altro operatore tra il momento " .
							" della tua lettura e il momento della tua scrittura." .
							" Sei pregato di ricaricare la pagina di modifica e" .
							" ripetere le modifiche che hai effettuato");
		}

	//***************************************************************************
	/**
	* 
	* @return string
	*/
	function array2xml($arraglio)
		{
		if (is_array($arraglio))
			{
			foreach($arraglio as $k => $v)
				{
				if (is_numeric($k))
					$k = "elemento_id_$k";
				$toret .= "<$k>";
				if (is_array($v))
					$toret .= $this->array2xml($v);
				else
					$toret .= htmlspecialchars($v);
				$toret .= "</$k>\n";
				}
			}
		
		return $toret;
		}

	//***************************************************************************
	/**
	* 
	* @return string
	*/
	function url_get_contents($url)
		{
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		$buffer = curl_exec($curl);
		curl_close($curl);		
		
		return $buffer;
		}

	//***************************************************************************
	/**
	 * restituisce il protocllo di connessione (http/s)
	*/
	function dammiProtocollo()
		{
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";

		return $protocol;
		}
		
	//*************************************************************************
	// ritorna l'url di un documento che ha una corrispondenza in una riga di db
	/**
	 * 
	 * @param waRecord $riga
	 * @param string $nome_campo
	 * @param string $tabella se vuoto prennde la tabella della chiave primaria, altrimenti la tabella data (evidentemente fk)
	 * @param string $idName se vuoto prennde il nome della chiave primaria del record; altrimenti il nome della chiave data (evidentemente fk)
	 * @return string
	 */
	function getUrlDoc(waRecord $riga, $nome_campo, $tabella = '', $idName = '')
		{
		if (!$riga->valore($nome_campo))
			return;
		
		$tabella = $tabella ? $tabella : $riga->righeDB->nomeTabella(0);
		$recPK = $idName ? $riga->valore($idName) : $riga->valore(0);
		$fs_doc = "$this->directoryDoc/$tabella/$nome_campo/$recPK." . 
					pathinfo($riga->$nome_campo, PATHINFO_EXTENSION);
		
		$params["t"] = $tabella;
		$params["c"] = $nome_campo;
		$params["k"] = $recPK;
		$params["e"] = pathinfo($riga->$nome_campo, PATHINFO_EXTENSION);
		
		return $this->dammiProtocollo() . 
				"://$this->dominio$this->httpwd" .
				"/$this->siglaSezione/downloaddoc.php" .
				"?p=" . base64_encode(serialize($params)) .
				"&t=" . @filemtime($fs_doc);
		}
		
	//*****************************************************************************
	// logga le scritture del database su file sequenziale; questa funzione e'
	// la callback per il DB driver (vedi parametro $WADB_LOG_CALLBACK_FNC in
	// dbconfig.inc.php
	/**
	 * @todo attenzione!
	 * 
	 * @param type $sql
	 * @return type
	 */
	function loggaScritturaDB($sql)
	    {
	    if (!defined("APPL_NOME_FILE_LOG_DB") || !APPL_NOME_FILE_LOG_DB)
	    	return;
	    	
	    $riga = array(
						date("Y-m-d H:i:s"),
						$this->utente['id'],
						$this->utente['email'] ,
						$_SERVER['REMOTE_ADDR'],
						$_SERVER['PHP_SELF'],
						$sql
					);
		
	    $fp = fopen(APPL_NOME_FILE_LOG_DB . "." . date("Ymd"), "a");
	    fputcsv($fp, $riga);
	    fclose($fp);
	    }
	    
	//***************************************************************************
	/** restituisce il nome di una location
	 * 
	 * @param type $latitude
	 * @param type $longitude
	 * @return stdClass
	 */
		
	function geoLocInversa($latitude, $longitude)
		{
		$localita = new stdClass();
		$url = APPL_MAPS_API_URL . "/geocode/json" .
				"?latlng=$latitude,$longitude" . 
				"&result_type=street_address" .
				"&key=" . APPL_MAPS_API_KEY;
		$data = $this->url_get_contents($url);
	//echo "<hr><pre>$data</pre>";			
		$data = json_decode($data);		

		foreach ($data->results[0]->address_components as $result)
			{
			if (in_array("street_number", $result->types))
				{
				$localita->civico = $result->long_name;
				}
			elseif (in_array("route", $result->types))
				{
				$localita->via = $result->long_name;
				}
			elseif (in_array("administrative_area_level_3", $result->types))
				{
				$localita->comune = $result->long_name;
				}
			elseif (in_array("administrative_area_level_2", $result->types))
				{
				$localita->provincia = $result->long_name;
				$localita->siglaProvincia = $result->short_name;
				}
			elseif (in_array("administrative_area_level_1", $result->types))
				{
				$localita->regione = $result->long_name;
				}
			elseif (in_array("country", $result->types))
				{
				$localita->stato = $result->long_name;
				$localita->siglaStato = $result->short_name;
				}
			elseif (in_array("postal_code", $result->types))
				{
				$localita->cap = $result->long_name;
				}
			}

			return $localita;
		}
		
	//***************************************************************************** 
	//***************************************************************************** 
	function loggaAccesso(waConnessioneDB $dbconn = null, $out = 0)
		{
		$dbconn = $dbconn ? $dbconn : $this->dammiConnessioneDB();
		$sql = "INSERT INTO access (id_user, section, flag_out, time, ip) VALUES (" .
					$dbconn->interoSql($this->utente["id"]) . "," .
					$dbconn->stringaSql($this->siglaSezione) . "," .
					$dbconn->interoSql($out) . "," .
					$dbconn->dataOraSql(time()) . "," .
					$dbconn->stringaSql($_SERVER['REMOTE_ADDR']) . ")";
		$this->eseguiDB($sql, $dbconn);
		
		}
		
	//*****************************************************************************
	/**
	* @return waSelezione_ext
	*/
	function modulo_dammiSelezioneExt(waModulo $modulo, 
								$campo, 
								$etichetta, 
								$solaLettura = false,
								$obbligatorio = false,
								$justify = true,
								$tbl = '',
								$keyFld = '',
								$descrFld = '',
								$flagBottoneModifica = false,
								$flagBottoneNuovo = false
			)
		{
		$ctrl = $modulo->aggiungiGenerico('waSelezione_ext', 
								$campo, $etichetta, $solaLettura, $obbligatorio);
		
		$ctrl->tabella = $tbl;
		$ctrl->campoChiave = $keyFld == '' ? $campo : $keyFld;
		$ctrl->campoDescrizione = $descrFld;
				
		if ($justify)
			$ctrl->larghezza = ($modulo->larghezza - $ctrl->sinistra) - $modulo->sinistraEtichette * 2;
			
		if ($flagBottoneModifica)
			$this->modulo_dammiBottoneModificaSelezione($ctrl);
		if ($flagBottoneNuovo)
			$this->modulo_dammiBottoneNuovoSelezione($ctrl);
		
		return $ctrl;
		}
			
//***************************************************************************
	} 	// fine classe recoci
	
//***************************************************************************


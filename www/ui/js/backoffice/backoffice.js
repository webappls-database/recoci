//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe wapagina
var backoffice = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: recoci,

	//-------------------------------------------------------------------------
	// proprieta'
	hoUnaPaginaAPerta: false,
	puntiPerMappa : [],		// array dei punti da mostrare nella mappa
	
	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();
				
		document.onkeyup = function onkeyup(event) {document.wapagina.intercettaKBShortcuts(event);}
		
		// resize automatico di eventuali finestre popup
//		if (window.opener && 
//			this.modulo != null && 
//			this.moduliIdx.length == 1 && 
//			this.tabella == null)
//			{
//				
//			var elems = this.modulo.obj.style.height.split("px");
////			window.innerHeight = (parseInt(elems[0]) + 80);
//			var h = (parseInt(elems[0]) + 180);
//			elems = this.modulo.obj.style.width.split("px");
////			window.innerWidth = (parseInt(elems[0]) + 40);
//			var w = (parseInt(elems[0]) + 40);
//			window.resizeTo(w, h);
//			}
		
		},
	
	//-------------------------------------------------------------------------
	// ridefiniamo la apri pagina per usare jquery e fancybox
	apriPagina: function (pagina)
	    {
		this.hoUnaPaginaAPerta = true;
		
		if (this.modalitaNavigazione == this.modalitaNavigazioneFinestra)
			return this.parent(pagina, window.innerWidth, window.innerHeight);
		
		jQuery("a.iframe").fancybox(
								{
								href : pagina,
								type : 'iframe',
								hideOnOverlayClick: false,
								width: '100%',
								height: '100%',
								speedIn	:	0, 
								speedOut :	0,
								padding: 0,
								margin: 20,
								onCleanup: function() 
											{
											if (self.frames[0] && 
													self.frames[0].document.wapagina &&
													self.frames[0].document.wapagina.chiudiPagina)
												return self.frames[0].document.wapagina.chiudiPagina();
											}
								}
								);
	
			jQuery("a.iframe").click();

		},
	    
	//-------------------------------------------------------------------------
	// ridefiniamo la chiudi pagina per usare jquery e fancybox
	chiudiPagina: function ()
	    {
		w = opener ? opener : parent;
		w.document.wapagina.hoUnaPaginaAPerta = false;
		if (this.modalitaNavigazione == this.modalitaNavigazioneFinestra)
			return this.parent();

		parent.jQuery.fancybox.close();
		return true;
		},
	    
	//-------------------------------------------------------------------------
	intercettaKBShortcuts: function (event)
	    {
		if (this.hoUnaPaginaAPerta)
			return;
	
		event = event ? event : window.event;
		
		if (event.keyCode == 78 && event.altKey)
			{
			// lasciamo la gestione alla finestra figlia
			var bottone = document.getElementById("Nuovo");
			if (bottone)
				bottone.click();
			}
		if (event.keyCode == 27)
			{
			if (this.modulo && this.modulo.controlli && this.modulo.controlli.cmd_annulla)
				this.modulo.controlli.cmd_annulla.obj.click();
			if (this.tabella && document.forms[this.tabella.nome + "_bottoniera"].Chiudi)
				document.forms[this.tabella.nome + "_bottoniera"].Chiudi.click();
				
			}
		},
		
	//-------------------------------------------------------------------------
	// azione di chiusura delle pagine figlie con tabelle
	azione_watabella_Chiudi: function ()
	    {
	    this.chiudiPagina();
	    },
	    
	//-------------------------------------------------------------------------
	// azione di export pdf
	azione_watabella_PDF: function ()
	    {
	    var w = open(location.href + (location.href .indexOf("?") != -1 ? "&" : "?") + "watbl_esporta_pdf[watabella]=1");
	    },
	    
	//-------------------------------------------------------------------------
	// azione di export csv
	azione_watabella_CSV: function ()
	    {
	    var w = open(location.href + (location.href .indexOf("?") != -1 ? "&" : "?") + "watbl_esporta_csv[watabella]=1");
	    },
	    
	//-------------------------------------------------------------------------
	// azione di export xls
	azione_watabella_XLS: function ()
	    {
	    var w = open(location.href + (location.href .indexOf("?") != -1 ? "&" : "?") + "watbl_esporta_xls[watabella]=1");
	    },
	    
	//-------------------------------------------------------------------------
	// mostra la pagina contenente la tabella senza paginazione
	azione_watabella_no_pagine: function ()
	    {
		var toGo = this.rimuoviParametroDaQS("watabella_no_pagine");
		var qoe = toGo.indexOf("?") != -1 ? "&" : "?";
		location.href = toGo + qoe + "watabella_no_pagine=1";
	    },
	    
	//-------------------------------------------------------------------------
	// mostra la pagina contenente la tabella con paginazione
	azione_watabella_pagine: function ()
	    {
		var toGo = this.rimuoviParametroDaQS("watabella_no_pagine");
		var qoe = toGo.indexOf("?") != -1 ? "&" : "?";
		location.href = toGo + qoe + "watabella_no_pagine=0";
	    },
	    
	//-------------------------------------------------------------------------
	// ovviamente questo metodo viene richiamato solo in situazioni standard
	// ossia quando c'e' un modulo che si chiama wamodulo e un bottone di
	// annullamento dell'editing che si chiama cmd_annulla; se siete in una
	// altra situzione dovete implementare la vostra gestione dell'evento
	evento_onclick_wamodulo_cmd_annulla: function (event)
	    {
	    this.chiudiPagina();
	    },
	    
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	// metodi per la gestione del popolamento dei controlli selezione/selezione_ext 
	// (bottoni nuovo/modifica)
	
	//-------------------------------------------------------------------------
	// da richiamare quando una selezione cambia il proprio valore: abilita
	// o disabilita il bottone di modifica dell'elemento
	selezione_change: function (nome_selezione)
		{
		this.modulo.controlli["btn_modifica_" + nome_selezione].abilita(
							this.modulo.controlli[nome_selezione].dammiValore()
							);
		},
		
	//-------------------------------------------------------------------------
	selezione_btn_nuovo_click: function (nome_selezione, nome_modulo, nome_dipende_da)
		{
		this.metodoAllineamento = this["allinea_" + nome_selezione];
	    var qoe = nome_modulo.indexOf("?") == - 1 ? "?" : "&";
		var pagina = nome_modulo + qoe + this.chiaveOperazione + "=" + this.opeInserimento;
		if (nome_dipende_da)
			pagina += "&" + nome_dipende_da + "=" + this.modulo.controlli[nome_dipende_da].dammiValore();
		this.apriPagina(pagina);
		},
		
	//-------------------------------------------------------------------------
	selezione_btn_modifica_click: function (nome_selezione, nome_modulo)
		{
		this.metodoAllineamento = this["allinea_" + nome_selezione];
	    var qoe = nome_modulo.indexOf("?") == - 1 ? "?" : "&";
		this.apriPagina(nome_modulo + qoe + this.chiaveOperazione + "=" + this.opeModifica +
							"&"  + nome_selezione + "=" + this.modulo.controlli[nome_selezione].dammiValore());
		},
		
	//-------------------------------------------------------------------------
	selezione_allinea: function (nome_selezione, datiInputFiglia, nome_funzione_rpc)
		{
		var ctrl = this.modulo.controlli[nome_selezione];
		var lista = this.modulo.RPC(nome_funzione_rpc);
		var nuovoValore;
		if (datiInputFiglia.cmd_invia)
			{
			nuovoValore = datiInputFiglia.idInserito ? datiInputFiglia.idInserito : ctrl.dammiValore();
			}
		else
			{
			nuovoValore = '';
			}
		
		ctrl.svuota();
		ctrl.riempi(lista, nuovoValore);

		// simuliamo la onchange, in modo che il bottone di modifica si
		// abiliti/disabiliti a seconda della situazione
		ctrl.simulaEvento("change");
		},
		
	//-------------------------------------------------------------------------
	// è differente dalla selezione normale in quanto
	// - la selezione normale deve ricaricare la lista lato server tramite RPC
	// - la selezione_ext preleva i due valori (id e testo) direttamente dai dati ricevuti lato client
	selezione_ext_allinea: function (nome_selezione, datiInputFiglia)
		{
		var ctrl = this.modulo.controlli[nome_selezione];
		if (datiInputFiglia.cmd_invia)
			{		
			ctrl.mettiValore(datiInputFiglia.idInserito ? datiInputFiglia.idInserito : ctrl.dammiValore(), datiInputFiglia.nome);
			}
		else
			{
			ctrl.mettiValore('', '');
			}
			
		// simuliamo la onchange, in modo che il bottone di modifica si
		// abiliti/disabiliti a seconda della situazione
		ctrl.simulaEvento("change");
		},
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	// fine metodi per la gestione del popolamento dei controlli selezione/selezione_ext 
	// (bottoni nuovo/modifica)
	//-------------------------------------------------------------------------
		
	//-------------------------------------------------------------------------
	vediHelp: function (controllo)
	    {
		var pagina = location.pathname.split('/').reverse()[0];
		var qs = encodeBase64(location.search);
		controllo = controllo ? controllo : '';
		this.apriPagina("help.php?pagina=" + pagina + "&controllo=" + controllo + "&qs=" + qs);
			
		},
		
	//-------------------------------------------------------------------------
	abilita_bottoni_submit: function (abilita)
		{
		if (this.modulo.controlli.cmd_invia)
			this.modulo.controlli.cmd_invia.mostra(abilita);
		if (this.modulo.controlli.cmd_invia_chiudi)
			this.modulo.controlli.cmd_invia_chiudi.mostra(abilita);
		if (this.modulo.controlli.cmd_annulla)
			this.modulo.controlli.cmd_annulla.mostra(abilita);
		if (this.modulo.controlli.cmd_elimina)
			this.modulo.controlli.cmd_elimina.mostra(abilita);

		},
		
	//-------------------------------------------------------------------------
	/**
	 * funzione di validazione standard di un modulo
	 */
	myValidaModulo: function() 
		{
		if (this.modulo.tipoInvioAnnulla)
			{
			this.modulo.obj.wamodulo_operazione.value = 5;
			return true;
			}
		if (this.modulo.tipoInvioElimina)
			{
			this.modulo.tipoInvioElimina = false;
			if (confirm('Confermi eliminazione?'))
				{
				this.modulo.obj.wamodulo_operazione.value = 4;
				return true;
				}
			return false;
			}
		if (this.modulo.verificaForma())
			{
			if(this.modulo.verificaObbligo())
				return true;
			}
		return false;
		},
		
	//-------------------------------------------------------------------------
	// valida modulo di default: disabilita i bottoni di submit
	evento_onsubmit_wamodulo_wamodulo: function (event)
		{
		this.abilita_bottoni_submit(false);

		if (this.myValidaModulo())
			return true
		
		this.abilita_bottoni_submit(true);
		return false;
		}
	
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------





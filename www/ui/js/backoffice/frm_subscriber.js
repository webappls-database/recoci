//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe wapagina
var wapagina = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: backoffice,

	//-------------------------------------------------------------------------
	// proprieta'
	datiInseriti	: false,

	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();

		var esito_inserimento = this.dammiParametroQS("esito_inserimento");
		if (esito_inserimento == "ok")
			jQuery('.esito_inserimento_ok').show(300).delay(5000).hide(500);	
		
		// carichiamo le select delle città
		jQuery(document).ready(function() {document.wapagina.cities_initialize()}); 
		},	

	//-------------------------------------------------------------------------
	chiudiPagina: function()
		{
		if (this.datiInseriti && !confirm("I dati inseriti nella presente scheda andranno perduti: confermi abbandono inserimento?"))
			return false;
		return this.parent();
		},
		
	//-------------------------------------------------------------------------
	cities_initialize: function()
		{
		if (!jQuery("select[name='id_city_birth']").length)
			return;
		
		var options = "";
		var optionsBO = "";
		var city = null;
		for (var i = 0; i < json_cities.length; i++)
			{
			city = json_cities[i];
			options += "<option value='" + city.id + "'>" + city.name;
			if (city.name.indexOf(" (BO)") != -1)
				optionsBO += "<option value='" + city.id + "'>" + city.name;
			}
		
		jQuery("select[name='id_city_birth']").append(options);
		jQuery("select[name='id_city_birth']").val(this.modulo.controlli.id_city_birth.valore);
		jQuery("select[name='id_city_residential']").append(optionsBO);
		jQuery("select[name='id_city_residential']").val(this.modulo.controlli.id_city_residential.valore);
		
		},
		
	//-------------------------------------------------------------------------
	verifica_cf: function()
		{
		this.datiInseriti = true;
		
		var ctrls = this.modulo.controlli;
		if (ctrls.name.dammiValore().trim() == "") return;
		if (ctrls.surname.dammiValore().trim() == "") return;
		if (ctrls.sex.dammiValore() == "") return;
		if (ctrls.birth_date.dammiValore() == false) return;
		if (ctrls.id_city_birth.dammiValore() == "") return;
		
		
		var retval = this.modulo.RPC("rpc_verificaCF", 
									ctrls.name.dammiValore().trim(),
									ctrls.surname.dammiValore().trim(),
									ctrls.sex.dammiValore(),
									ctrls.birth_date.dammiValore().getTime() / 1000,
									ctrls.id_city_birth.dammiValore()
								);
			
		ctrls.tax_code.mettiValore(retval["cf"]);
		if (retval["esito"] != "ok")
			{
			var msg = retval["descrizione_errore"] + "\n\n" +
						" Ricontrolla i dati inseriti, oppure forza la registrazione" +
						" spuntando la casella Forzatura; ricorda che" +
						" l'operazione sarà tracciata.";
			alert(msg);
			}
			
		},
		
	//-------------------------------------------------------------------------
	evento_onchange_wamodulo_name: function()
		{
		this.verifica_cf();
		},
		
	//-------------------------------------------------------------------------
	evento_onchange_wamodulo_surname: function()
		{
		this.verifica_cf();
		},
		
	//-------------------------------------------------------------------------
	evento_onchange_wamodulo_sex: function()
		{
		this.verifica_cf();
		},
		
	//-------------------------------------------------------------------------
	evento_onchange_wamodulo_birth_date: function()
		{
		var birth_date = this.modulo.controlli.birth_date.dammiValore();
		if (!birth_date) 
			{
			alert("Attenzione: data non valida")
			return this.modulo.controlli.birth_date.mettiValore(false);
			}
		var now = new Date();
		var anni_18_fa = new Date(now.getFullYear() - 18, now.getMonth(), now.getDate());
		if (birth_date.getTime() > anni_18_fa.getTime())
			{
			alert("Attenzione: la data indicata è relativa a un minorenne");
			return this.modulo.controlli.birth_date.mettiValore(false);
			}
			
		this.verifica_cf();
		},
		
	//-------------------------------------------------------------------------
	evento_onchange_wamodulo_id_city_birth: function()
		{
		this.verifica_cf();
		},
		
	//-------------------------------------------------------------------------
	//------------ ultracustom!!!! dentro selezione_ext.js si verifica --------
	//------------ se esiste questo metodo; se esiste viene invocato al -------
	//------------ posto di quello standard rpc                         -------
	//-------------------------------------------------------------------------
	dammiLista_id_city_birth: function(testoDaCercare)
		{

		var retval = {};
		var city = null;
		var added = 0;
		for (var i = 0; i < json_cities.length; i++)
			{
			city = json_cities[i];
			if (city.name.toLowerCase().indexOf(testoDaCercare.toLowerCase()) != -1)
				{
				retval[city.id] = city.name;
				added++;
				if (added >= 50)
					break;
				}
			}
			
		return retval;
		},
		
	//-------------------------------------------------------------------------
	dammiLista_id_city_residential: function(testoDaCercare)
		{

		var retval = {};
		var city = null;
		for (var i = 0; i < json_cities.length; i++)
			{
			city = json_cities[i];
			if (city.name.toLowerCase().indexOf(testoDaCercare.toLowerCase()) != -1 &&
				city.name.indexOf(" (BO)") != -1)
				{
				retval[city.id] = city.name;
				}
			}
			
		return retval;
		},
		
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.wapagina = new wapagina();

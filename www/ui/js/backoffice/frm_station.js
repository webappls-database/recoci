//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe wapagina
var wapagina = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: backoffice,

	//-------------------------------------------------------------------------
	//initialization
	initialize: function()
		{
		this.parent();

		
		// carichiamo le select delle città
		jQuery(document).ready(function() {document.wapagina.cities_initialize()}); 
		},	

	//-------------------------------------------------------------------------
	cities_initialize: function()
		{
		if (!jQuery("select[name='id_city']").length)
			return;
		
		var optionsBO = "";
		var city = null;
		for (var i = 0; i < json_cities.length; i++)
			{
			city = json_cities[i];
			if (city.name.indexOf(" (BO)") != -1)
				optionsBO += "<option value='" + city.id + "'>" + city.name;
			}
		
		jQuery("select[name='id_city']").append(optionsBO);
		jQuery("select[name='id_city']").val(this.modulo.controlli.id_city.valore);
		
		},
		
	//-------------------------------------------------------------------------
	//------------ ultracustom!!!! dentro selezione_ext.js si verifica --------
	//------------ se esiste questo metodo; se esiste viene invocato al -------
	//------------ posto di quello standard rpc                         -------
	//-------------------------------------------------------------------------
	//-------------------------------------------------------------------------
	dammiLista_id_city: function(testoDaCercare)
		{

		var retval = {};
		var city = null;
		for (var i = 0; i < json_cities.length; i++)
			{
			city = json_cities[i];
			if (city.name.toLowerCase().indexOf(testoDaCercare.toLowerCase()) != -1 &&
				city.name.indexOf(" (BO)") != -1)
				{
				retval[city.id] = city.name;
				}
			}
			
		return retval;
		},
		
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.wapagina = new wapagina();

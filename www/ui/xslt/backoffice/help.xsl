<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:str="http://exslt.org/strings" exclude-result-prefixes="str"
				xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl"
>

<xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes" /> 

<!-- ********************************************************************** -->
<!--  ************* template della pagina ********************************* -->
<!-- ********************************************************************** -->
<xsl:template match="waapplicazione">
	<xsl:variable name="dati" select="pagina/elementi/elemento[nome = 'dati']/valore" />
	
	<html>
		<head>
			<link href='{waapplicazione_path}/uis/wa_default/css/waapplicazione.css' rel='stylesheet'/><xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/mootools/1.2.5/mootools-yui-compressed.js'></script><xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{waapplicazione_path}/uis/wa_default/js/strmanage.js'></script><xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{waapplicazione_path}/uis/wa_default/js/waapplicazione.js'></script><xsl:text>&#10;</xsl:text>

			<!--js fancybox-->
			<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script><xsl:text>&#10;</xsl:text>
			<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.pack.js"></script><xsl:text>&#10;</xsl:text>
			<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.easing-1.4.pack.js"></script><xsl:text>&#10;</xsl:text>
			<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.mousewheel-3.0.4.pack.js"></script><xsl:text>&#10;</xsl:text>
			<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.css" type="text/css" media="screen" /><xsl:text>&#10;</xsl:text>
			
			<!--siccome usiamo sia mootols che jquery, jquery deve essere chiamato per esteso-->
			<script type="text/javascript">
				jQuery.noConflict();			
			</script>
			<xsl:text>&#10;</xsl:text>
			
			<title>
				<xsl:value-of select="$dati/titolo_pagina" /> - <xsl:value-of select="titolo" /> - help online
		    </title>
		</head>
		
		<body onunload="document.wapagina.chiudiFiglia()" class="pagina_help">
			<noscript>
				<hr />
				<div style='text-align: center'>
					<b>
						Questa applicazione usa Javascript, ma il tuo browser ha questa funzione
						disabilitata. Sei pregato di abilitare Javascript per il dominio <xsl:value-of select="dominio" />
						e ricaricare la pagina.
					</b>
				</div>
				<hr />
			</noscript>
			
			<!-- se lavoriamo con navigazione interna creiamo anche l'iframe destinato a contenere la finestra figlia-->
			<xsl:text>&#10;</xsl:text>
			<xsl:if test="modalita_navigazione = '3'">
				<a class="iframe" id="fb_iframe" href="" style="display:none;"></a>
			</xsl:if>
	
			<!-- titolo -->
			<xsl:text>&#10;</xsl:text>
			<div class="titolo">
				Help online
				<br />
				<br />
				<xsl:value-of select="$dati/titolo_pagina"/>
				<xsl:if test="$dati/etichetta != ''">
					<br /> 
					<xsl:value-of select="$dati/etichetta"/>
				</xsl:if>
			</div>
				
			<!-- testo -->
			<xsl:text>&#10;</xsl:text>
			<div class="testo">
				<xsl:value-of disable-output-escaping="yes" select="$dati/help"/>
			</div>
				
			<!-- dati del controllo/campo -->
			<xsl:if test="$dati/tipo != ''" >
				<table>
					<tr>
						<td style="font-weight: bold;">
							tipo controllo
						</td>
						<td>
							<xsl:value-of select="$dati/tipo"/>
						</td>
					<tr>
					</tr>
						<td style="font-weight: bold;">
							abilitato
						</td>
						<td>
							<xsl:choose>
								<xsl:when test="$dati/solaLettura = 1">no</xsl:when>
								<xsl:otherwise>si</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td style="font-weight: bold;">
							obbligatorio
						</td>
						<td>
							<xsl:choose>
								<xsl:when test="$dati/obbligatorio = 1">si</xsl:when>
								<xsl:otherwise>no</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					
					<xsl:if test="$dati/lunghezza != '' and ($dati/tipo_controllo = 'testo' or $dati/tipo_controllo = 'intero' or $dati/tipo_controllo = 'valuta')" >
						<tr>
						<td style="font-weight: bold;">
								lunghezza massima
							</td>
							<td>
								<xsl:value-of select="$dati/lunghezza"/> caratteri
							</td>
						</tr>
					</xsl:if>
					
				</table>
			</xsl:if>
			
			<form>
				<xsl:if test="pagina/elementi/elemento[nome = 'operatore_puo_editare']/valore = '1'" >
					<input type="button" onclick="location.href='{pagina/uri}&amp;edit=1'" value="Modifica" alt="Modifica" title="Modifica" />
				</xsl:if>
				
				<input type="button" onclick="document.wapagina.chiudiPagina()" value="Chiudi" alt="Chiudi" title="Chiudi" />
			</form>
				
			<!-- dati versione -->
			<xsl:if test="pagina/elementi/elemento[nome = 'dati_versione']" >
				<xsl:call-template name="dati_versione">
					<xsl:with-param name="valore" select="pagina/elementi/elemento[nome = 'dati_versione']/valore"/>
				</xsl:call-template>
			</xsl:if>
				
			<!-- tentativi euristici: qui l'xsl tenta sempre di caricare:-->
			<!-- - un css dell'applicazione (directory_di_lavoro/ui/css/nome_applicazione.css)-->
			<!-- - un css della sezione (directory_di_lavoro/ui/css/{sigla_sezione}/{sigla_sezione}.css)-->
			<!-- - un css della pagina  (directory_di_lavoro/ui/css/nome_pagina.css)-->
			<!-- - un js dell'applicazione (directory_di_lavoro/ui/js/nome_applicazione.js)-->
			<!-- - un js della sezione (directory_di_lavoro/ui/js/{sigla_sezione}/{sigla_sezione}.js)-->
			<!-- - un js della pagina  (directory_di_lavoro/ui/js/nome_pagina.js)-->
			<!-- i js della pagina sono sempre gli ultimi a dover essere caricati, altrimenti non vedono le strutture altrui... -->
			<link href='{directory_lavoro}/ui/css/{nome}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<link href='{directory_lavoro}/ui/css/{sigla_sezione}/{sigla_sezione}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<link href='{directory_lavoro}/ui/css/{sigla_sezione}/{pagina/nome}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{nome}.js'></script>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{sigla_sezione}/{sigla_sezione}.js'></script>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{sigla_sezione}/{pagina/nome}.js'></script>
			<xsl:text>&#10;</xsl:text>

			<!-- se non esiste il file js relativo alla pagina, creiamo un oggetto pagina che ha le proprieta' -->
			<!-- e i metodi di default dell'applicazione. -->
			<!-- In ogni caso diciamo all'applicazione/pagina in che modalita' si dovra' navigare -->
			<!-- e se la pagina deve allineare la mamma e/o eventualmente chiudersi -->
			<script type='text/javascript'>
				if (!document.wapagina)
					document.wapagina = new backoffice();
				document.wapagina.modalitaNavigazione = '<xsl:value-of select="modalita_navigazione" />';
				<xsl:if test="pagina/ritorno/valori">
					document.wapagina.allineaGenitore('<xsl:value-of select="pagina/ritorno/valori" />');
					<xsl:if test="pagina/ritorno/chiudi">
						document.wapagina.chiudiPagina();
					</xsl:if>
				</xsl:if>
			</script><xsl:text>&#10;</xsl:text>
			
			<!--siccome usiamo sia mootols che jquery, jquery deve essere chiamato per esteso-->
			<script type="text/javascript">
				jQuery(document).ready(function() {
					jQuery(".fancybox").fancybox();
				});
			</script>

		</body>
	</html>
</xsl:template>

<!-- ********************************************************************** -->
<!-- template dati -->
<!-- ********************************************************************** -->
<xsl:template name="dati_versione">
	<xsl:param name="valore" />
	
	<div class="waapplicazione_dati_versione">
		<xsl:value-of select="/waapplicazione/titolo" />
		- versione <xsl:value-of select="$valore/nr" />
		del <xsl:value-of select="$valore/data" />
	</div>
	
</xsl:template>

<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:str="http://exslt.org/strings" exclude-result-prefixes="str"
				xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl"
>

<xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes" /> 

<!-- ********************************************************************** -->
<!--  ************* template della pagina ********************************* -->
<!-- ********************************************************************** -->
<xsl:template match="waapplicazione">
	<html>
		<head>
			<link href='{waapplicazione_path}/uis/wa_default/css/waapplicazione.css' rel='stylesheet'/><xsl:text>&#10;</xsl:text>
			<!--<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/mootools/1.2.5/mootools-yui-compressed.js'></script><xsl:text>&#10;</xsl:text>-->
			<script type='text/javascript' src='{directory_lavoro}/ui/js/libs/mootools/1.3.2/mootools-yui-compressed.js'></script><xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{waapplicazione_path}/uis/wa_default/js/strmanage.js'></script><xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{waapplicazione_path}/uis/wa_default/js/waapplicazione.js'></script><xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/libs/tinymce/4.0.21/tinymce.min.js'></script><xsl:text>&#10;</xsl:text>

			<!--js fancybox-->
			<!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script><xsl:text>&#10;</xsl:text>-->
			<script type="text/javascript" src="{directory_lavoro}/ui/js/libs/jquery/1.4.4/jquery.min.js"></script><xsl:text>&#10;</xsl:text>
			<script type="text/javascript" src="{directory_lavoro}/ui/js/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.pack.js"></script><xsl:text>&#10;</xsl:text>
			<script type="text/javascript" src="{directory_lavoro}/ui/js/libs/fancybox/1.3.4/jquery.easing-1.4.pack.js"></script><xsl:text>&#10;</xsl:text>
			<script type="text/javascript" src="{directory_lavoro}/ui/js/libs/fancybox/1.3.4/jquery.mousewheel-3.0.4.pack.js"></script><xsl:text>&#10;</xsl:text>
			<link rel="stylesheet" href="{directory_lavoro}/ui/js/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.css" type="text/css" media="screen" /><xsl:text>&#10;</xsl:text>
			
			<!--siccome usiamo sia mootols che jquery, jquery deve essere chiamato per esteso-->
			<script type="text/javascript">
				jQuery.noConflict();			
			</script>
			<xsl:text>&#10;</xsl:text>
			
			<title>
		    	<xsl:value-of select="titolo" />
				<xsl:if test="pagina/elementi/elemento[nome='titolo']" >
					-
					<xsl:value-of select="pagina/elementi/elemento[nome='titolo']/valore" />
				</xsl:if>
		    </title>
			
		</head>
		<xsl:variable name="classe_body">
			<!--se siamo in una finestra senza menu, allora siamo in una finestar figlia: il-->
			<!--body ha una sua classe particolare-->
			<xsl:if test="not(pagina/elementi/elemento[nome = 'wamenu'])" >finestra_figlia</xsl:if>
		</xsl:variable>
		<body onunload="document.wapagina.chiudiFiglia()" class="{$classe_body}">
			<noscript>
				<hr />
				<div style='text-align: center'>
					<b>
						Questa applicazione usa Javascript, ma il tuo browser ha questa funzione
						disabilitata. Sei pregato di abilitare Javascript per il dominio <xsl:value-of select="dominio" />
						e ricaricare la pagina.
					</b>
				</div>
				<hr />
			</noscript>
			
			<!--hacking per ie8-->
			<xsl:text>&#10;</xsl:text>
			<xsl:text>&#10;</xsl:text>
			<xsl:text disable-output-escaping="yes">&lt;!--</xsl:text>[if lt IE 9]<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
			<style>
				html {font-size: 16px;}
				body {width: 80%;}
			</style>
			<xsl:text>&#10;</xsl:text>
			<xsl:text disable-output-escaping="yes">&lt;![endif]--&gt;</xsl:text>			
			<xsl:text>&#10;</xsl:text>
			<xsl:text>&#10;</xsl:text>
			
			<!--messaggio di inserimento ok-->
			<div class="esito_inserimento_ok">
				Registazione avvenuta correttamente
			</div>
			
			<!-- se lavoriamo con navigazione interna creiamo anche l'iframe destinato a contenere la finestra figlia-->
			<xsl:text>&#10;</xsl:text>
			<xsl:if test="modalita_navigazione = '3'">
				<!--	<iframe id='waapplicazione_iframe_figlia' class='waapplicazione_iframe_figlia' style='visibility:hidden'>
				</iframe>-->
				
				<a class="iframe" id="fb_iframe" href="" style="display:none;"></a>
			</xsl:if>
	
			<!-- creazione degli elementi fissi costitutivi della pagina (titolo, menu, ecc.-->
			<xsl:if test="pagina/elementi/elemento[nome = 'wamenu']" >
				<xsl:call-template name="elemento_pagina">
					<xsl:with-param name="elemento" select="pagina/elementi/elemento[nome = 'wamenu']"/>
				</xsl:call-template>
			</xsl:if>
				
			<xsl:if test="pagina/elementi/elemento[nome = 'titolo']" >
				<xsl:call-template name="titolo_pagina">
					<xsl:with-param name="valore" select="pagina/elementi/elemento[nome = 'titolo']/valore"/>
				</xsl:call-template>
			</xsl:if>
				
			<!-- tutti gli altri elementi della pagina in ordine di creazione -->
			<xsl:for-each select="pagina/elementi/elemento[
													nome != 'titolo' 
													and nome != 'wamenu'
													and nome != 'dati_versione'
													and nome != 'telefono_supporto'
													and nome != 'azione_torna'
													and nome != 'azione_chiudi'
													]" >
					<xsl:call-template name="elemento_pagina">
						<xsl:with-param name="elemento" select="."/>
					</xsl:call-template>
			</xsl:for-each>
			
			<xsl:if test="pagina/elementi/elemento[nome = 'azione_torna']" >
				<xsl:call-template name="azione_torna" />
			</xsl:if>
				
			<xsl:if test="pagina/elementi/elemento[nome = 'azione_chiudi']" >
				<xsl:call-template name="azione_chiudi" />
			</xsl:if>
				
			<xsl:if test="pagina/elementi/elemento[nome = 'dati_versione']" >
				<xsl:call-template name="dati_versione">
					<xsl:with-param name="valore" select="pagina/elementi/elemento[nome = 'dati_versione']/valore"/>
				</xsl:call-template>
			</xsl:if>
				
			<xsl:if test="pagina/elementi/elemento[nome = 'telefono_supporto']" >
				<xsl:call-template name="telefono_supporto">
					<xsl:with-param name="valore" select="pagina/elementi/elemento[nome = 'telefono_supporto']/valore"/>
				</xsl:call-template>
			</xsl:if>
				
				
			<div class="waapplicazione_logo">
				<img src='../ui/img/nettuno-vota-consultazioni-300x214.png'/>
			</div>

				
			<!-- tentativi euristici: qui l'xsl tenta sempre di caricare:-->
			<!-- - un css dell'applicazione (directory_di_lavoro/ui/css/nome_applicazione.css)-->
			<!-- - un css della sezione (directory_di_lavoro/ui/css/{sigla_sezione}/{sigla_sezione}.css)-->
			<!-- - un css della pagina  (directory_di_lavoro/ui/css/nome_pagina.css)-->
			<!-- - un js dell'applicazione (directory_di_lavoro/ui/js/nome_applicazione.js)-->
			<!-- - un js della sezione (directory_di_lavoro/ui/js/{sigla_sezione}/{sigla_sezione}.js)-->
			<!-- - un js della pagina  (directory_di_lavoro/ui/js/nome_pagina.js)-->
			<!-- i js della pagina sono sempre gli ultimi a dover essere caricati, altrimenti non vedono le strutture altrui... -->
			<link href='{directory_lavoro}/ui/css/{nome}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<link href='{directory_lavoro}/ui/css/{sigla_sezione}/{sigla_sezione}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<xsl:text>&#10;</xsl:text>
			<link href='{directory_lavoro}/ui/css/{sigla_sezione}/{pagina/nome}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{nome}.js'></script>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{sigla_sezione}/{sigla_sezione}.js'></script>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{sigla_sezione}/{pagina/nome}.js'></script>
			<xsl:text>&#10;</xsl:text>

			<!--cities-->
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{sigla_sezione}/json_cities.js'></script>
			<xsl:text>&#10;</xsl:text>

			<!-- se non esiste il file js relativo alla pagina, creiamo un oggetto pagina che ha le proprieta' -->
			<!-- e i metodi di default dell'applicazione. -->
			<!-- In ogni caso diciamo all'applicazione/pagina in che modalita' si dovra' navigare -->
			<!-- e se la pagina deve allineare la mamma e/o eventualmente chiudersi -->
			<script type='text/javascript'>
				if (!document.wapagina)
					document.wapagina = new backoffice();
				document.wapagina.modalitaNavigazione = '<xsl:value-of select="modalita_navigazione" />';
				<xsl:if test="pagina/ritorno/valori">
					document.wapagina.allineaGenitore('<xsl:value-of select="pagina/ritorno/valori" />');
					<xsl:if test="pagina/ritorno/chiudi">
						document.wapagina.chiudiPagina();
					</xsl:if>
				</xsl:if>
			</script><xsl:text>&#10;</xsl:text>
			
		</body>
	</html>
</xsl:template>

<!-- ********************************************************************** -->
<!-- template elementi pagina -->
<!-- ********************************************************************** -->
<xsl:template name="elemento_pagina">
	<xsl:param name="elemento" />

	<xsl:text>&#10;</xsl:text>
	<div class="waapplicazione_{$elemento/nome}" id="waapplicazione_{$elemento/nome}">
		<xsl:value-of disable-output-escaping="yes" select="$elemento/valore" />
	</div>
	<xsl:text>&#10;</xsl:text>

</xsl:template>

<!-- ********************************************************************** -->
<!-- template dati -->
<!-- ********************************************************************** -->
<xsl:template name="dati_versione">
	<xsl:param name="valore" />
	
	<div class="waapplicazione_dati_versione">
		<xsl:value-of select="/waapplicazione/titolo" />
		- versione <xsl:value-of select="$valore/nr" />
		del <xsl:value-of select="$valore/data" />
	</div>
	
</xsl:template>

<!-- ********************************************************************** -->
<!-- template telefono_supporto -->
<!-- ********************************************************************** -->
<xsl:template name="telefono_supporto">
	<xsl:param name="valore" />
	
	<div class="waapplicazione_telefono_supporto">
		supporto telefonico: <xsl:value-of select="$valore" />
	</div>
	
</xsl:template>

<!-- ********************************************************************** -->
<!-- template titolo pagina -->
<!-- ********************************************************************** -->
<xsl:template name="titolo_pagina">
	<xsl:param name="valore" />

	<xsl:text>&#10;</xsl:text>
	<div class="waapplicazione_titolo">
		<!--<xsl:attribute name='onclick'>document.wapagina.vediHelp()</xsl:attribute>-->
		<xsl:variable name="righe" select="exsl:node-set(str:tokenize($valore, '&#10;'))" />
		<xsl:for-each select="$righe">
			<xsl:if test="position() = 1" >
				<xsl:value-of select="." />
			</xsl:if>
			<xsl:if test="position() &gt; 1" >
				<br />
				<b><xsl:value-of select="." /></b>
			</xsl:if>
		</xsl:for-each>
	</div>
	<xsl:text>&#10;</xsl:text>

</xsl:template>

<!-- ********************************************************************** -->
<!-- template titolo azione_torna -->
<!-- ********************************************************************** -->
<xsl:template name="azione_torna">
	
	<div class="waapplicazione_azione_torna">
		<form><input type='button' value='&lt;&lt; Torna' onclick='history.back()' /></form>
	</div>

</xsl:template>

<!-- ********************************************************************** -->
<!-- template titolo azione_chiudi -->
<!-- ********************************************************************** -->
<xsl:template name="azione_chiudi">
	
	<div class="waapplicazione_azione_chiudi">
		<form><input type='button' value='Chiudi' onclick='document.wapagina.chiudiPagina()' /></form>
	</div>

</xsl:template>


<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" 
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
				xmlns:str="http://exslt.org/strings" exclude-result-prefixes="str"
				xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl"
>

<xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes" /> 

<!-- ********************************************************************** -->
<!--  ************* template della pagina ********************************* -->
<!-- ********************************************************************** -->
<xsl:template match="waapplicazione">
	<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			
		    <title>
		    	<xsl:value-of select="titolo" />
				<xsl:if test="pagina/elementi/elemento[nome='titolo']" >
					-
					<xsl:value-of select="pagina/elementi/elemento[nome='titolo']/valore" />
				</xsl:if>
		    </title>
		</head>
		<body>
			<!-- tutti gli altri elementi della pagina in ordine di creazione -->
			<xsl:for-each select="pagina/elementi/elemento">
				<xsl:call-template name="elemento_pagina">
					<xsl:with-param name="elemento" select="."/>
				</xsl:call-template>
			</xsl:for-each>
			
			<!-- tentativi euristici: qui l'xsl tenta sempre di caricare:-->
			<!-- - un css dell'applicazione (directory_di_lavoro/ui/css/nome_applicazione.css)-->
			<!-- - un css della sezione (directory_di_lavoro/ui/css/{sigla_sezione}/{sigla_sezione}.css)-->
			<!-- - un css della pagina  (directory_di_lavoro/ui/css/nome_pagina.css)-->
			<!-- - un js dell'applicazione (directory_di_lavoro/ui/js/nome_applicazione.js)-->
			<!-- - un js della sezione (directory_di_lavoro/ui/js/{sigla_sezione}/{sigla_sezione}.js)-->
			<!-- - un js della pagina  (directory_di_lavoro/ui/js/nome_pagina.js)-->
			<!-- i js della pagina sono sempre gli ultimi a dover essere caricati, altrimenti non vedono le strutture altrui... -->
			<link href='{directory_lavoro}/ui/css/{nome}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<link href='{directory_lavoro}/ui/css/{sigla_sezione}/{sigla_sezione}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<link href='{directory_lavoro}/ui/css/{sigla_sezione}/{pagina/nome}.css' rel='stylesheet'/>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{sigla_sezione}/{sigla_sezione}.js'></script>
			<xsl:text>&#10;</xsl:text>
			<script type='text/javascript' src='{directory_lavoro}/ui/js/{sigla_sezione}/{pagina/nome}.js'></script>
			<xsl:text>&#10;</xsl:text>

		</body>
	</html>
</xsl:template>

<!-- ********************************************************************** -->
<!-- template elementi pagina -->
<!-- ********************************************************************** -->
<xsl:template name="elemento_pagina">
	<xsl:param name="elemento" />

	<xsl:text>&#10;</xsl:text>
	<div class="waapplicazione_{$elemento/nome}">
		<xsl:value-of disable-output-escaping="yes" select="$elemento/valore" />
	</div>
	<xsl:text>&#10;</xsl:text>

</xsl:template>

<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
<!-- ********************************************************************** -->
</xsl:stylesheet>
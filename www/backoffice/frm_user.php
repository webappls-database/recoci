<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//*****************************************************************************
include "backoffice.inc.php";

//*****************************************************************************
class frm_user extends backoffice
	{
	/**
	 *
	 * @var waModulo
	 */
	var $modulo;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true, true);
		
		
		$this->creaModulo();

		if ($this->modulo->daAggiornare())
			{
			$this->aggiornaRecord();
			}
		elseif ($this->modulo->daEliminare())
			{
			$this->eliminaRecord($this->modulo->record);
			}
		else
			{
			$this->mostraPagina();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il modulo e la manda in output
	* @return void
	*/
	function mostraPagina()
		{
		$contesto = $this->dammiContestoTitolo("station");
		$this->aggiungiElemento("Scheda operatore $contesto", "titolo");
		$this->aggiungiElemento($this->modulo);
		$this->mostra();
			
		}
		
	//***************************************************************************
	function creaModulo()
		{
		$this->modulo = $this->dammiModulo();
		$this->modulo->righeDB = $this->dammiRecordset();
		$riga = $this->modulo->righeDB->righe[0];
		$solaLettura = false;

		if (!$_GET["id_station"])
			{
			$this->modulo->aggiungiSelezione("id_station", "Seggio", $solaLettura, true)
				->sql = "select id, name from station" .
						" where not deleted order by name";
			}		
			
		$this->modulo->aggiungiTesto("surname", "Cognome", $solaLettura, true);
		$this->modulo->aggiungiTesto("name", "Nome", $solaLettura, true);
		$this->modulo->aggiungiEmail("email", "E-Mail", $solaLettura, true);
		$this->modulo->aggiungiTesto("phone", "Telefono", $solaLettura);
		
		$this->modulo->aggiungiLogico("supervisor", "Supervisore", $solaLettura);
		
		$this->modulo->aggiungiAreaTesto("notes", "Note", $solaLettura);
		$this->modulo->aggiungiLogico("invia_credenziali", "Spedisci credenziali via email", $solaLettura);
		$this->modulo_bottoniSubmit($this->modulo, $solaLettura, $riga ? true : false);

		$this->modulo->leggiValoriIngresso();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waRigheDB
	*/
	function dammiRecordset()
		{
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT *" .
				" FROM user" .
				" WHERE id=" . $dbconn->interoSql($_GET['id']) . 
				" AND NOT deleted";
			
		$righeDB = $this->dammiRigheDB($sql, $dbconn, $_GET['id'] ? 1 : 0);
		if ($_GET['id'] && !$righeDB->righe)
			$this->mostraMessaggio("Record non trovato", "Record non trovato", false, true);
		
		return $righeDB;
		}
		
	//***************************************************************************
	function aggiornaRecord()
		{
		// controlli obbligatorieta' e formali
		$this->verificaObbligo($this->modulo);
		
		$riga = $this->modulo->righeDB->righe[0];
		if (!$riga)
			{
			$riga = $this->modulo->righeDB->aggiungi();
			$riga->inserisciValore("pwd", $this->encryptPassword($this->getPassword()));
			$riga->creation_time = time();
			}
		else 
			{
			$this->verificaViolazioneLock($this->modulo);
			}
			
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$dbconn->iniziaTransazione();
		// prevalorizziamo gruppo con quanto passato come parametro; se
		// non e' stato passato nulla nessun problema: il salvataggio del modulo
		// ricoprirà l'effetto di queste valorizzazione
		$riga->id_station = $_GET["id_station"];
		$this->setEditorData($riga);
		$this->modulo->salva();
		$this->salvaRigheDB($riga->righeDB);
		$idInserito = $dbconn->ultimoIdInserito();
		$dbconn->confermaTransazione();
		if ($this->modulo->invia_credenziali)
			{
			$this->inviaMailPassword($riga);
			}

		$valoriRitorno = $idInserito ? array_merge(array("idInserito" => $idInserito), $this->modulo->input) : $this->modulo->input;
		$this->ritorna($valoriRitorno);
		}
		
	//***************************************************************************
	function inviaMailPassword(waRecord $riga)
		{
		// il server di next-data non ci dice se siamo sotto https o meno; per
		// capirlo usiamo HTTP_X_FORWARDED_FOR, che pero' non ci da una garanzia 
		// assoluta (dipende da come e' configurato il dominio nel pannello di 
		// controllo
		$protocol = $this->dammiProtocollo();

		
		$cr = "\r\n";
		$body = "Ciao $riga->name,$cr$cr" . 
				"queste sono le credenziali mediante cui potrai accedere a $this->titolo" . 
				" ($protocol://$this->dominio$this->httpwd/):$cr$cr" .
				
				"Login:    $riga->email$cr" . 
				"Password: " . $this->decryptPassword($riga->pwd) . "$cr$cr" .
				
				"Una volta all'interno del programma potrai" .
				" modificare la tua password accedendo a Servizi->Profilo.$cr$cr".
				
				"Questa mail si genera automaticamente, non rispondere o scrivere" .
				" a questo indirizzo mail. Per necessità scrivi all'indirizzo" .
				" di assistenza ($this->emailSupporto).$cr$cr" .
				
				"Buon lavoro!$cr$cr" . 
				
				"lo staff tecnico $cr" .
				$this->titolo . " $cr";
		
		if (!$this->mandaMail($riga->email, "Credenziali accesso " . $this->titolo, $body))
			$this->mostraMessaggio("Errore invio email", 
									"Attenzione: si e' verificato" .
									" un errore durante l'invio del messaggio email contenente le credenziali di accesso.<p>" .
									"Sei pregato di avvisare l'assistenza tecnica e ripetere l'operazione" .
									" quando l'inconveniente sara' stato risolto.", false, true);
		}
	
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new frm_user();

<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
include "backoffice.inc.php";

$appl = new backoffice(true, false);
$appl->loggaAccesso(null, 1);
$appl->datiSessione = array();
$appl->ridireziona($appl->paginaLogin);

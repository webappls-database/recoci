<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//******************************************************************************
include "backoffice.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class tbl_station extends backoffice
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->aggiungiElemento($this->dammiMenu());
		$this->aggiungiElemento("Elenco seggi", "titolo");
		$this->aggiungiElemento($this->dammiTabella());
		$this->mostra();
		}

	//*****************************************************************************
	/**
	 * @return waTabella
	 */
	function dammiTabella()
		{
		// creazione della tabella
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT station.*," .
			" city.city_name as city_name" .
			" FROM station" .
			" join city on station.id_city=city.id" .
			" WHERE NOT station.deleted" .
			" ORDER BY station.name";
		
		$tabella = parent::dammiTabella($sql);
		$tabella->paginaModulo = "frm_station.php";
		if (!$this->utenteSupervisore())
			{
			$tabella->eliminaAzione ("Nuovo");
			$tabella->azioni["Modifica"]->funzioneAbilitazione = array($this, "recordModificabile");
			$tabella->azioni["Elimina"]->funzioneAbilitazione = array($this, "recordModificabile");
			}
				
		$tabella->aggiungiAzione("Operatori", true);
		$tabella->aggiungiAzione("Iscritti", true);
		
		$tabella->aggiungiColonna("id", "ID", false, false, false)->aliasDi = "station.id";
		$tabella->aggiungiColonna("name", "Nome")->aliasDi = "station.name";
		$tabella->aggiungiColonna("city_name", "Località")->aliasDi = "city.city_name";
		$col = $tabella->aggiungiColonna("email", "E-mail");
			$col->aliasDi = "station.email";
			$col->link = true;
		$tabella->aggiungiColonna("phone", "Telefono")->aliasDi = "station.phone";
			
		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$tabella->caricaRighe()) $this->mostraErroreDB($tabella->righeDB->connessioneDB);

		return $tabella;
		}

	//*****************************************************************************
	function recordModificabile(waTabella $tabella)
		{
		return $this->utenteSupervisore();
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new tbl_station();
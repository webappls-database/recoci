<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
include_once "backoffice.inc.php";

//*****************************************************************************
class downloaddoc extends backoffice
	{
		
	//***************************************************************************
	function mostraPagina()
		{
		// per vedere qualsiasi documento occorre essere loggati (è giusto?)
		 if (!$this->utente) return;
		
		$params = unserialize(base64_decode($_GET["p"]));
		$filename = "$this->directoryDoc/$params[t]/$params[c]/$params[k].$params[e]";

		if (!is_readable($filename)) return;
		$basename = basename($filename);
		
		header("Pragma: ");
		header("Expires: Fri, 15 Aug 1980 18:15:00 GMT");
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0, false");
		header('Content-Length: '. filesize($filename));
		if (function_exists("mime_content_type"))
			$mime = mime_content_type($filename);
		else
			{
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			$mime = $finfo->file($filename);
			}

		if ($mime)
			{
			header("Content-Type: $mime");
			header("Content-Disposition: inline; filename=\"$basename\""); 
			}
		else 
			{
			header("Content-Disposition: attachment; filename=\"$basename\";" );
			header("Content-Type: application/force-download");
			header("Content-Transfer-Encoding: binary");
			}
			
		readfile($filename);
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
$page = new downloaddoc();
$page->mostraPagina();


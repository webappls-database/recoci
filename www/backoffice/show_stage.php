<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//******************************************************************************
include "backoffice.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class pagina extends backoffice
	{
		

	//*****************************************************************************
	function show()
		{
		$dbconn = $this->dammiConnessioneDB();

		// cerchiamo i points
		$sql = "select stage.*," .
				" route.name as route_name," .
				" user.name as user_name," .
				" user.surname as user_surname" .
				" from stage" .
				" join route on stage.route_id=route.id" .
				" join user on route.user_id=user.id" .
				" where stage.id=" . $dbconn->interoSql($_GET["id"]);
		
		$stage = $this->dammiRigheDB($sql, $dbconn)->righe[0];
		if (!$stage)
			$this->mostraMessaggio ("Nessun record", "Nessun record", false, false);
				
		$this->faccelaVedere($stage);
		}

	//*****************************************************************************
	function faccelaVedere(waRecord $stage)
		{
		header("Content-Type: text/html; charset=utf-8");			
		
		?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<style type="text/css">
					
					body
						{
						background-color: #f0f0f0;
						}
						
					div
						{
						margin-top: 20px;
						}
						
				</style>

				<script type="text/javascript">
				</script>

			</head>
			<body>
				
				<div id="route_name">
					Itinerario:
					<?php echo $stage->route_name ?>
				</div>
				
				<div id="name">
					Tappa:
					<?php echo $stage->name ?>
				</div>
				
				<div id="description">
					Descrizione:
					<?php echo $stage->description ?>
				</div>
				
				<div id="reverse_geolocation">
					Ubicazione:
					<?php echo $stage->reverse_geolocation ?>
				</div>
				
				<div id="user_name">
					Creato da:
					<?php echo $stage->user_name . " " . $stage->user_surname?>
				</div>
				
				<div id="creation_time">
					Creato il:
					<?php echo date("d/m/Y", $stage->creation_time)?> 
					alle :
					<?php echo date("H:i", $stage->creation_time)?> 
				</div>
				
				<div id="image">
					<img src="<?php echo $this->getUrlDoc($stage, "image")?>">
				</div>
				
			</body>
		</html>



		<?php
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
$pagina = new pagina();
$pagina->show();
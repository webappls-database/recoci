<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//******************************************************************************
include "backoffice.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class tbl_subscriber extends backoffice
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		$this->finestraFiglia = (bool) $_GET["id_station"];

		$this->aggiungiElemento($this->dammiMenu());
		$contesto = $this->dammiContestoTitolo("station");
		$this->aggiungiElemento("Elenco iscrizioni $contesto", "titolo");
		$this->aggiungiElemento($this->dammiTabella());
		$this->mostra();
		}

	//*****************************************************************************
	/**
	 * @return waTabella
	 */
	function dammiTabella()
		{
		// creazione della tabella
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT subscriber.*," .
			" station.name as station_name," .
			" concat(user.name, ' ', user.surname) as user_name," .
			" IF(subscriber.forced, 'si', 'no') AS s_forced" .
			" FROM subscriber" .
			" join station on subscriber.id_station=station.id" .
			" join user on subscriber.id_user=user.id" .
			" WHERE NOT subscriber.deleted" .
			($_GET["id_station"] ? " and subscriber.id_station=" . $dbconn->interoSql($_GET["id_station"]) : '') .
			" ORDER BY station_name, subscriber.name";
		
		$tabella = parent::dammiTabella($sql);
		$tabella->paginaModulo = "frm_subscriber.php?id_station=$_GET[id_station]";
		
		if ($this->utenteSupervisore())
			$tabella->aggiungiAzione("CSV");
		$tabella->azioni["Modifica"]->funzioneAbilitazione = array($this, "recordModificabile");
		$tabella->azioni["Elimina"]->funzioneAbilitazione = array($this, "recordModificabile");
		
		$tabella->aggiungiColonna("id", "ID", false, false, false)->aliasDi = "subscriber.id";
		$tabella->aggiungiColonna("surname", "Cognome")->aliasDi = "subscriber.surname";
		$tabella->aggiungiColonna("name", "Nome")->aliasDi = "subscriber.name";
		$tabella->aggiungiColonna("creation_time", "Data/Ora")->aliasDi = "subscriber.creation_time";

		$col = $tabella->aggiungiColonna("s_forced", "Forzatura", true, true, true, WATBL_ALLINEA_CENTRO);
			$col->aliasDi = "IF(subscriber.forced, 'si', 'no')";
		$tabella->aggiungiColonna("user_name", "Operatore")->aliasDi = "concat(user.name, ' ', user.surname)";
		if (!$_GET["id_station"])
			{
			$tabella->aggiungiColonna("station_name", "Seggio")->aliasDi = "station.name";
			}
			
		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$tabella->caricaRighe()) $this->mostraErroreDB($tabella->righeDB->connessioneDB);

		return $tabella;
		}

	//*****************************************************************************
	function recordModificabile(waTabella $tabella)
		{
		return $this->utenteSupervisore();
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new tbl_subscriber();
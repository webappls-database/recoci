<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//******************************************************************************
include "backoffice.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class tbl_city extends backoffice
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->aggiungiElemento($this->dammiMenu());
		$this->aggiungiElemento("Elenco città", "titolo");
		$this->aggiungiElemento($this->dammiTabella());
		$this->mostra();
		}

	//*****************************************************************************
	/**
	 * @return waTabella
	 */
	function dammiTabella()
		{
		// creazione della tabella
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT city.*," .
				" new_code.city_code_land as new_code_city_code_land," .
				" new_code.city_name as new_code_city_name" .
			" FROM city" .
			" left join city as new_code on city.city_moved_code_istat=new_code.city_code_istat" .
			" WHERE NOT city.deleted" .
			" ORDER BY city.city_region, city.city_province_code, city.city_name";
		
		$tabella = parent::dammiTabella($sql);
		$tabella->eliminaAzione("Nuovo");
		$tabella->eliminaAzione("Modifica");
		$tabella->eliminaAzione("Elimina");
		
		$tabella->aggiungiColonna("id", "ID", false, false, false)->aliasDi = "city.id";
		$tabella->aggiungiColonna("city_region", "Regione")->aliasDi = "city.city_region";
		$tabella->aggiungiColonna("city_province_code", "Provincia")->aliasDi = "city.city_province_code";
		$tabella->aggiungiColonna("city_name", "Nome")->aliasDi = "city.city_name";
		$tabella->aggiungiColonna("city_code_land", "Codice ISTAT")->aliasDi = "city.city_code_land"
				;
		$col = $tabella->aggiungiColonna("city_moved", "Spostato");
			$col->allineamento = WATBL_ALLINEA_CENTRO;
			$col->aliasDi = "city.city_moved";
		$col = $tabella->aggiungiColonna("new_code_city_code_land", "Nuovo codice ISTAT");
			$col->aliasDi = "new_code.city_code_land";
		$col = $tabella->aggiungiColonna("new_code_city_name", "Nuovo nome");
			$col->aliasDi = "new_code.city_name";
			
		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$tabella->caricaRighe()) $this->mostraErroreDB($tabella->righeDB->connessioneDB);

		return $tabella;
		}

	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new tbl_city();
<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//*****************************************************************************
include "backoffice.inc.php";

//*****************************************************************************
class frm_station extends backoffice
	{
	/**
	 *
	 * @var waModulo
	 */
	var $modulo;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true, true);
		
		
		$this->creaModulo();

		if ($this->modulo->daAggiornare())
			{
			$this->aggiornaRecord();
			}
		elseif ($this->modulo->daEliminare())
			{
			$this->eliminaRecord($this->modulo->record);
			}
		else
			{
			$this->mostraPagina();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il modulo e la manda in output
	* @return void
	*/
	function mostraPagina()
		{
		$this->aggiungiElemento("Scheda seggio", "titolo");
		$this->aggiungiElemento($this->modulo);
		$this->mostra();
			
		}
		
	//***************************************************************************
	function creaModulo()
		{
		$this->modulo = $this->dammiModulo();
		$this->modulo->righeDB = $this->dammiRecordset();
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$riga = $this->modulo->righeDB->righe[0];
		$solaLettura = false;

		$this->modulo->aggiungiTesto("name", "Nome", $solaLettura, true);
		if ($this->preferenzeUtente["selezione_ext"])
			$this->modulo_dammiSelezioneExt($this->modulo, "id_city", "Località", $solaLettura, true, true, "city", 'id', "concat (city_name, ' (', city_province_code, ')')");
		else
			$this->modulo->aggiungiSelezione("id_city", "Località", $solaLettura, true);

		$this->modulo->aggiungiEmail("email", "E-Mail", $solaLettura);
		$this->modulo->aggiungiTesto("phone", "Telefono", $solaLettura);
			
		$this->modulo->aggiungiAreaTesto("notes", "Note", $solaLettura);
		$this->modulo_bottoniSubmit($this->modulo, $solaLettura, $riga ? true : false);

		$this->modulo->leggiValoriIngresso();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waRigheDB
	*/
	function dammiRecordset()
		{
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT *" .
				" FROM station" .
				" WHERE id=" . $dbconn->interoSql($_GET['id']) . 
				" AND NOT deleted";
			
		$righeDB = $this->dammiRigheDB($sql, $dbconn, $_GET['id'] ? 1 : 0);
		if ($_GET['id'] && !$righeDB->righe)
			$this->mostraMessaggio("Record non trovato", "Record non trovato", false, true);
		
		return $righeDB;
		}
		
	//***************************************************************************
	function aggiornaRecord()
		{
		// controlli obbligatorieta' e formali
		$this->verificaObbligo($this->modulo);
		
		$riga = $this->modulo->righeDB->righe[0];
		if (!$riga)
			{
			$riga = $this->modulo->righeDB->aggiungi();
			$riga->creation_time = time();
			}
		else 
			{
			$this->verificaViolazioneLock($this->modulo);
			}
			
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$dbconn->iniziaTransazione();
		$this->setEditorData($riga);
		$this->modulo->salva();
		$this->salvaRigheDB($riga->righeDB);
		$idInserito = $dbconn->ultimoIdInserito();
		$dbconn->confermaTransazione();

		$valoriRitorno = $idInserito ? array_merge(array("idInserito" => $idInserito), $this->modulo->input) : $this->modulo->input;
		$this->ritorna($valoriRitorno);
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new frm_station();

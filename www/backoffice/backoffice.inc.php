<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//****************************************************************************************
include_once (dirname(__FILE__) . "/../recoci.inc.php");


//****************************************************************************************
class backoffice extends recoci
	{
	var $preferenzeUtente;
	
	var $paginaIniziale = 'tbl_subscriber.php';
	var $paginaLogin = 'frm_login.php';
	
	// indica se stiamo facendo vedere qualcosa in una finestra figlia
	var $finestraFiglia = false;

	//****************************************************************************************
	/**
	* costruttore
	 * 
	 */
	function __construct($verificaUtente = true, $soloSupervisore = false)
		{
		$this->titoloSezione = "Backoffice";
		$this->siglaSezione = "backoffice";
		
		parent::__construct();

		$this->datiSessione = &$_SESSION[$this->siglaSezione];

		$this->utente = &$this->datiSessione['utente'];

		// impostazioni delle preferenze
        $this->preferenzeUtente = unserialize(base64_decode($_COOKIE[$this->nome . "_$this->siglaSezione" . "_prefs"]));
		$this->preferenzeUtente["navigazione_finestre"] = isset($this->preferenzeUtente["navigazione_finestre"]) ? $this->preferenzeUtente["navigazione_finestre"] : true;
		$this->preferenzeUtente["max_righe_tabella"] = $this->preferenzeUtente["max_righe_tabella"] ? $this->preferenzeUtente["max_righe_tabella"] : WATBL_LISTA_MAX_REC;		
		$this->preferenzeUtente["azioni_tabella"] = isset($this->preferenzeUtente["azioni_tabella"]) ? $this->preferenzeUtente["azioni_tabella"] : "sx_default";
		$this->preferenzeUtente["selezione_ext"] = isset($this->preferenzeUtente["selezione_ext"]) ? $this->preferenzeUtente["selezione_ext"] : true;
		$this->preferenzeUtente["data_inline"] = isset($this->preferenzeUtente["data_inline"]) ? $this->preferenzeUtente["data_inline"] : true;
		$this->modalitaNavigazione = $this->preferenzeUtente["navigazione_finestre"] ? WAAPPLICAZIONE_NAV_FINESTRA : WAAPPLICAZIONE_NAV_INTERNA;

		// verifica validità dell'utent loggato
		$this->verificaUtente($verificaUtente, $soloSupervisore);
		
		}
	
	//***************************************************************************
	/**
	* @return void
	*/
	function dammiMenu()
		{
		if ($this->finestraFiglia)
			{
			return false;
			}

		$m = new waMenu();
		$m->apri();

		$m->apriSezione("Iscrizioni", "tbl_subscriber.php");
		$m->chiudiSezione();

		if ($this->utenteSupervisore())
			{
			$m->apriSezione("Tabelle");
				$m->aggiungiVoce("Operatori", "tbl_user.php");
				$m->aggiungiVoce("Seggi", "tbl_station.php");
				$m->aggiungiVoce("Città", "tbl_city.php");
			$m->chiudiSezione();
			}
			
		$m->apriSezione("Servizi");
			$m->aggiungiVoce("Profilo", "javascript:document.wapagina.apriPagina(\"frm_config.php\")");
			$m->aggiungiVoce("Logout", "logout.php");
		$m->chiudiSezione();
			
		$m->chiudi();
		return $m;
			
		}
		
	//*****************************************************************************
	// verifica l'utente loggato
	//*****************************************************************************
	/**
	 * 
	 * @param boolean $verificaUtente se l'utente deve essere vwrificato
	 * @param boolean $soloSupervisore se solo un supervisore puo' accedere alla pagina
	 */
	function verificaUtente($verificaUtente, $soloSupervisore)
		{
		if ($verificaUtente)
			{
			if (!$this->utente)
				{
				$this->ridireziona($this->paginaLogin);
				}
				
			elseif ($soloSupervisore && !$this->utenteSupervisore())
				{
				$this->mostraMessaggio("Accesso non abilitato", "Accesso non abilitato");
				}
			}
		
		}
		
	//*****************************************************************************
	// verifica forza e età della password; se debole o scaduta lo mandiamo
	// al cambio password
	//*****************************************************************************
	function verificaValiditaPassword()
		{
		if (basename($_SERVER["PHP_SELF"]) == "frm_config.php")
			return true;
		
		if (!$this->verificaForzaPassword($this->decryptPassword($this->utente["pwd"])))
			$this->ridireziona ("frm_config.php?rinnovo=1");
		
		if ($this->utente["pwdmodifytime"] + APPL_GIORNI_VALIDITA_PWD*60*60*24 < mktime(0,0,0))
			$this->ridireziona ("frm_config.php?rinnovo=1");
		
		}
		
	//*************************************************************************
	// questa verifica di fatto ha senso solo per i supervisori, ossia coloro
	// che possono accedere al backoffice e vedere dati personali altrui
	function verificaForzaPassword($pwd)
	    {
	    if (strlen($pwd) < 8) return false;
	    if (strlen($pwd) > 12) return false;
	       
		// la password non deve contenere il nome o il cognome
		if (stripos($pwd, $this->utente["name"]) !== false ||
			stripos($pwd, $this->utente["surname"]) !== false)
			return false;

		return true;
	    }
           
	//*************************************************************************
	// questa verifica di fatto ha senso solo per i supervisori, ossia coloro
	// che possono accedere al backoffice e vedere dati personali altrui
	function verificaNuovaPassword($pwd, $pwdConferma)
	    {
	    if ($pwd != $pwdConferma) return false;
		
		if (!$this->verificaForzaPassword($pwd))
			return false;

		// la vecchia password, se c'è, è encryptata
	    if ($this->encryptPassword($pwd) == $this->utente["pwd"]) 
			return false;
	    
		return true;
	    }
           
	//*****************************************************************************
	// crea il blocco dei controlli standard per un dato di tipo coordinata
	//*****************************************************************************
	function modulo_coordinate(waModulo $modulo, $solaLettura, $obbligatorio = false, $suffix = '')
		{
		$suffix = $suffix ? "_$suffix" : "";
		$ctrl = $this->modulo->aggiungiValuta("latitude$suffix", "Latitudine $suffix", $solaLettura, $obbligatorio);
			$ctrl->nrInteri = 2;
			$ctrl->nrDecimali = 14;
		$ctrl = $this->modulo->aggiungiValuta("longitude$suffix", "Longitudine $suffix", $solaLettura, $obbligatorio);
			$ctrl->nrInteri = 2;
			$ctrl->nrDecimali = 14;
		
		}
		
	//*****************************************************************************
	// crea il blocco dei bottoni standard di una form
	//*****************************************************************************
	function modulo_bottoniSubmit(waModulo $modulo, 
									$solaLettura = false, 
									$showDeleteButton = true,
									$cmdOkCaption = 'Registra', 
									$cmdCancelCaption = 'Annulla', 
									$cmdDeleteCaption = 'Elimina',
									$cmdCloseCaption = 'Chiudi',
									$buttonWidth = 120,
									$separatore = true)
		{
		$modulo->aggiungiNonControllo("separatore_sopra_bottoniera");
		$ultimo_controllo = $modulo->controlli[count($modulo->controlli) - 1];
		$top = $ultimo_controllo->alto + $ultimo_controllo->altezza +
				$modulo->altezzaLineaControlli + $modulo->interlineaControlli;
		$nextButtonLeft = $modulo->sinistraControlli;
		if (!$solaLettura)
			{
			$okCtrl = new waBottone($modulo, 'cmd_invia', $cmdOkCaption);
			$okCtrl->alto = $top;
			$okCtrl->sinistra = $nextButtonLeft;
			$okCtrl->larghezza = $buttonWidth;
			$okCtrl->altezza = $buttonHeight;
			$nextButtonLeft = $okCtrl->sinistra + $okCtrl->larghezza + 1;
			}
		if ($solaLettura)
			$cancelCtrl = new waBottone($modulo, 'cmd_annulla', $cmdCloseCaption);
		else 
			$cancelCtrl = new waBottone($modulo, 'cmd_annulla', $cmdCancelCaption);
		$cancelCtrl->alto = $top;
		$cancelCtrl->sinistra = $nextButtonLeft;
		$cancelCtrl->larghezza = $buttonWidth;
		$cancelCtrl->altezza = $buttonHeight;
		$cancelCtrl->annulla = TRUE;
		$cancelCtrl->invia = false;
		$nextButtonLeft = $cancelCtrl->sinistra + $cancelCtrl->larghezza + 1;
	
		if ($showDeleteButton && ! $solaLettura)
			{
			$ctrl = new waBottone($modulo, 'cmd_elimina', $cmdDeleteCaption);
			$ctrl->alto = $top;
			$ctrl->sinistra = $nextButtonLeft;
			$ctrl->larghezza = $buttonWidth;
			$ctrl->altezza = $buttonHeight;
			$ctrl->elimina = TRUE;
			$nextButtonLeft = $ctrl->sinistra + $ctrl->larghezza + 1;
			}
			
		if ($separatore)
			$modulo->aggiungiNonControllo("separatore_sotto_bottoniera");
		
		return $nextButtonLeft;
		
		}
			
	//***************************************************************************
	/**
	* -
	*/
	function dammiTitoloPagina()
		{
		foreach ($this->elementi as $elemento)
			{
			if ($elemento["nome"] == "titolo")
				return $elemento["valore"];
			}
		}
		
	//***************************************************************************
	/**
	* -
	* @return waTabella
	*/
	function dammiTabella($sqlOArray)
		{
		$table = new waTabella($sqlOArray, $this->fileConfigDB);
		$table->applicazione = $this;
		$tipoXslt = $this->preferenzeUtente["azioni_tabella"] ? $this->preferenzeUtente["azioni_tabella"] : 'context';
		$table->xslt = dirname(__FILE__) . "/../ui/xslt/$this->siglaSezione/watabella_azioni_$tipoXslt.xsl";
		$table->eliminaAzione("Vedi");
		
		// determinazione del nr di righe massimo della tabella
		if ($_GET['watabella_no_pagine'])
			{
			// anche se chiedono di vedere tutto in  una sola pagina, limitiamo 
			// comunque la vista a 1000 righe, altrimenti rischiano di 
			// impallare il server
			$table->listaMaxRec = 1000;
			$table->aggiungiAzione("pagine", false, "Pagine");
			}
		else
			{
			$table->listaMaxRec = $this->preferenzeUtente["max_righe_tabella"] ? 
									$this->preferenzeUtente["max_righe_tabella"] : 
									WATBL_LISTA_MAX_REC;
			$table->aggiungiAzione("no_pagine", false, "No pagine");
			}
			
		$table->titolo = $this->dammiTitoloPagina();
		$table->pdf_orientazione = "L";
		
		if ($this->finestraFiglia)
			{
			$table->aggiungiAzione("Chiudi");
//			if (!$this->preferenzeUtente["navigazione_finestre"])
//				$table->azioni['Chiudi']->etichetta = "&lt; Torna";
				
			// portiamo il bottone "chiudi in prima posizione
			$swappo['Chiudi'] = $table->azioni['Chiudi'];
			foreach ($table->azioni as $k => $v)
				{
				if ($k != "Chiudi")
					$swappo[$k] = $v;
				}
			$table->azioni = $swappo;
			}
			
		return $table;
		}
		
	//***************************************************************************
	/**
	* manda in output la pagina
	* 
	*/
	function mostra($bufferizza = false)
		{
		$dati_versione = array("nr" => $this->versione, "data" => date("d/m/Y", $this->dataVersione));
		$this->aggiungiElemento($this->array2xml($dati_versione), "dati_versione", "XML");
		$this->aggiungiElemento($this->telSupporto, "telefono_supporto");
			
		parent::mostra($bufferizza);
		}
		
	//*************************************************************************
	// definisce l'url del documento all'interno di un controllo waCaricaFile
	function setUrlDoc(waCaricaFile $ctrl)
		{
		$riga = $ctrl->modulo->righeDB->righe[0];
		if ($riga && $riga->valore($ctrl->nome))
			{
			$ctrl->paginaVisualizzazione =  $this->getUrlDoc($riga, $ctrl->nome);
			}
		}
		
	//***************************************************************************** 
	// elimina eventuali documenti salvati inprecedenza
	//***************************************************************************** 
	function eliminaDoc(waCaricaFile $ctrl)
		{
		$rs = $ctrl->modulo->righeDB;
		$id = $rs->righe[0] ? $rs->righe[0]->valore(0) : $rs->connessioneDB->ultimoIdInserito();
		$pattern = "$this->directoryDoc/" . $rs->nomeTabella(0) . "/$ctrl->nome/$id.*";
		$files = glob($pattern);
		if ($files)
			{
			foreach ($files as $file)
				@unlink($file);
			}
		}
		
	//***************************************************************************** 
	// salva l'eventuale documento allegato
	//***************************************************************************** 
	function salvaDoc(waCaricaFile $ctrl)
		{
		// salvataggio del documento (se c'e'...)
		if ($ctrl->daEliminare())
			// cancelliamo un eventuale documento esistente
			$this->eliminaDoc($ctrl);
		elseif ($ctrl->erroreCaricamento())			
	    	$this->mostraMessaggio("Errore caricamento file", 
	    						"Si e' verificato l'errore " . 
	    						$ctrl->erroreCaricamento() . 
	    						" durante il caricamento del documento $ctrl->nome." .
	    						" Si prega di avvertire l'assistenza tecnica.", 
    							false, true);
		elseif ($ctrl->daSalvare())
			{
			$this->eliminaDoc($ctrl);
			$rs = $ctrl->modulo->righeDB;
			$dest = "$this->directoryDoc/" . $rs->nomeTabella(0);
			@mkdir($dest);
			$dest .=  "/$ctrl->nome";
			@mkdir($dest);
			
			$id = $rs->righe[0] ? $rs->righe[0]->valore(0) : $rs->connessioneDB->ultimoIdInserito();
			$dest = "$dest/$id." . pathinfo($ctrl->valoreInput, PATHINFO_EXTENSION);
			if (!$ctrl->salvaFile($dest))
	    		$this->mostraMessaggio("Errore spostamento file", 
	    						"Si e' verificato un errore " . 
	    						" durante lo spostamento del documento $ctrl->nome." .
	    						" Si prega di avvertire l'assistenza tecnica.", 
    							false, true);
			}
			
		return true;			
		}
	
	//*****************************************************************************
	// trasforma un record in una struttura xml
	function record2xml(waRecord $riga)
		{
		$rs = $riga->righeDB;
		for ($i = 0; $i < $rs->nrCampi(); $i++)
			{
			if (stripos($rs->nomeCampo($i), "pwd") === false)
				{
				$toret .= "<" . strtolower($rs->nomeCampo($i)) . ">";
				if ($rs->tipoCampo($i) == WADB_DATA)
					$toret .= date("d/m/Y", $riga->valore($i));
				elseif ($rs->tipoCampo($i) == WADB_DATAORA)
					$toret .= date("d/m/Y H.i.s", $riga->valore($i));
				else
					$toret .= htmlspecialchars($riga->valore($i));
				$toret .= "</" . strtolower($rs->nomeCampo($i)) . ">\n";
				}
			}
		
		return $toret;
		}
		
	//*****************************************************************************
	// trasforma un recordset in una struttura xml
	function rs2xml(waRigheDB $rs)
		{
		foreach ($rs->righe as $riga)
			{
			$toret .= "<riga>\n";
			$toret .= $this->record2xml($riga);
			$toret .= "</riga>\n";
			}
		
		return $toret;
		}
		
	//***************************************************************************
	/**
	* -
	*/
	function dammiContestoTitolo($tabella, $nome_campo_descrittore = "name")
		{
		/**
		 * @todo sistemare
		 */
		$key = $_GET["id_$tabella"];
		if (!$key)	return "";
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT $nome_campo_descrittore FROM $tabella WHERE id=" . $dbconn->interoSql($key);
		$riga = $this->dammiRigheDB($sql, $dbconn, 1)->righe[0];
		if (!$riga)	return "";
		
		return "\n" . $riga->valore(0);
		
		}

	//***************************************************************************
	/**
	* -
	*/
	function utenteSupervisore()
		{
		return (bool) $this->utente["supervisor"];
		}

	//**************************************************************************
	/**
	 * crea un controllo di tipo AreaTesto_ext, con relativa etichetta, all'interno
	 * del modulo
	 * 
	 * @param string	$nome nome del controllo e dell'etichetta che si intende creare
	 * @param string	$etichetta valore dell'etichetta del controllo
	 * @param boolean	$solaLettura true se il controllo va creato in sola lettura
	 * @param boolean	$obbligatorio true se il controllo va creato obbligatorio
	 * @param int		$altoEtichetta posizionamento verticale dell'etichetta; per default e' uguale al posizionamento dell'etichetta precedente a cui viene aggiunto il valore di {@link $interlineaControlli}
	 * @param int		$altoControllo posizionamento verticale del controllo; per default e' uguale al posizionamento del controllo di oinput precedente a cui viene aggiunto il valore di {@link $interlineaControlli}
	 * @param int		$sinistraEtichetta posizionamento orizzontale dell'etichetta; per default e' uguale a {@link $sinistraEtichette}
	 * @param int		$sinistraControllo posizionamento orizzontale del controolo; per default e' uguale a {@link $sinistraControlli}
	 * 
	 * @return waTesto
	 */
	function aggiungiAreaTesto_ext(waModulo $modulo, 
									$nome, $etichetta, 
									$solaLettura = false, 
									$obbligatorio = false, 
									$altoEtichetta = false, 
									$altoControllo = false, 
									$sinistraEtichetta = false, 
									$sinistraControllo = false)
		{
		$ctrl = $modulo->aggiungiGenerico('waAreaTesto_ext', $nome, $etichetta, 
											$solaLettura, $obbligatorio, 
											$altoEtichetta, $altoControllo, 
											$sinistraEtichetta, $sinistraControllo);
		$ctrl->altezza = 170;
		$modulo->giustificaControllo($ctrl, false);
		return $ctrl;
		}

//***************************************************************************
	} 	// fine classe backoffice
	
//***************************************************************************


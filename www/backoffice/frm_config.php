<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
include "backoffice.inc.php";

//*****************************************************************************
class frm_config extends backoffice
	{
	/**
	 *
	 * @var waModulo
	 */
	var $modulo;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true, false);
		
		$this->creaModulo();

		if ($this->modulo->daAggiornare())
			{
			$this->aggiornaRecord();
			}
		else
			{
			$this->mostraPagina();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il modulo e la manda in output
	* @return void
	*/
	function mostraPagina()
		{
		$this->aggiungiElemento("Profilo e impostazioni", "titolo");
		$this->aggiungiElemento($this->modulo);
		$this->mostra();
			
		}
		
	//***************************************************************************
	function creaModulo()
		{
		
		$this->modulo = $this->dammiModulo();
		$this->modulo->righeDB = $this->dammiRecordset();
		$riga = $this->modulo->righeDB->righe[0];
		$solaLettura = false;
		
		$this->modulo->aggiungiTesto("name", "Nome", true);
		$this->modulo->aggiungiTesto("surname", "Cognome", true);
		$this->modulo->aggiungiTesto("email", "Email", true);
		$this->modulo->aggiungiTesto("station_name", "Seggio", true);
		$this->modulo->aggiungiTesto("phone", "Telefono", $solaLettura);
		
		$ctrlP = $this->modulo->aggiungiPassword("new_pwd", "Password", $solaLettura, true);
		$ctrl = $this->modulo->aggiungiPassword("pwd_conferma", "Password per conferma", $solaLettura, true);
			$ctrlP->corrispondenzaDB = $ctrl->corrispondenzaDB = false;
			$ctrlP->valore = $ctrl->valore = $this->decryptPassword($riga->pwd);
			$ctrlP->caratteriMax = $ctrl->caratteriMax = 12;

		$ctrl = $this->modulo->aggiungiLogico("navigazione_finestre", "Navigazione a finestre", false);
			$ctrl->corrispondenzaDB = false;
			$ctrl->valore = $this->preferenzeUtente["navigazione_finestre"];
			
		$ctrl = $this->modulo->aggiungiSelezione("max_righe_tabella", "Nr. righe tabella per pagina", false, true, false);
			$ctrl->corrispondenzaDB = false;
			$ctrl->rigaVuota = false;
			for ($i = 10; $i <= 100; $i += 10)
				$ctrl->lista[$i] = $i;
			$ctrl->valore = $this->preferenzeUtente["max_righe_tabella"];

		$ctrl = $this->modulo->aggiungiSelezione("azioni_tabella", "Azioni su righe tabella", false, true, false);
			$ctrl->corrispondenzaDB = false;
			$ctrl->rigaVuota = false;
			$ctrl->lista = array("sx_default" => "bottoni a sinistra", "context" => "menu contestuale", "dx" => "bottoni a destra");
			$ctrl->valore = $this->preferenzeUtente["azioni_tabella"];
		
		$ctrl = $this->modulo->aggiungiLogico("selezione_ext", "Liste a suggerimento", false);
			$ctrl->corrispondenzaDB = false;
			$ctrl->valore = $this->preferenzeUtente["selezione_ext"];
			
		$ctrl = $this->modulo->aggiungiLogico("data_inline", "Date in linea", false);
			$ctrl->corrispondenzaDB = false;
			$ctrl->valore = $this->preferenzeUtente["data_inline"];
			
		$this->modulo_bottoniSubmit($this->modulo, false, false);
		
		$this->modulo->leggiValoriIngresso();

		}

	//***************************************************************************
	/**
	* -
	*
	* @return waRigheDB
	*/
	function dammiRecordset()
		{
		$dbconn = $this->dammiConnessioneDB();
		$sql = "select user.*," .
				" station.name as station_name," .
				" station.id_city" .
				" from user" .
				" join station on user.id_station=station.id" .
				" where user.id=" . $dbconn->interoSql($this->utente['id']);
			
		$righeDB = $this->dammiRigheDB($sql, $dbconn, 1);
		if (!$righeDB->righe)
			{
			$this->mostraMessaggio("Record non trovato", "Record non trovato", false, true);
			}

		return $righeDB;
		}
		
	//***************************************************************************
	function aggiornaRecord()
		{
		$this->verificaObbligo($this->modulo);

		
		$riga = $this->modulo->righeDB->righe[0];
		if (!$this->modulo->new_pwd || ($this->modulo->new_pwd != $this->modulo->pwd_conferma))
			{
			// non è supervisore: controlliamo solo che le password siano uguali
			$this->mostraMessaggio("Password errata", "Password vuota o non corrispondenti");
			}
			
		$this->modulo->salva();
		$riga->pwd  =  $this->encryptPassword($this->modulo->new_pwd);
		$this->setEditorData($riga);
		$this->salvaRigheDB($riga->righeDB);
		$this->utente = $this->record2Array($riga);
		
		// blob della configurazione da mandare via cookie al browser
		$prefs["navigazione_finestre"] = $_POST["navigazione_finestre"] ? 1 : 0;
		$prefs["max_righe_tabella"] = $_POST["max_righe_tabella"];
		$prefs["azioni_tabella"] = $_POST["azioni_tabella"];
		$prefs["selezione_ext"] = $_POST["selezione_ext"] ? 1 : 0;
		$prefs["data_inline"] = $_POST["data_inline"] ? 1 : 0;
		$cookieVal = base64_encode(serialize($prefs));
		setcookie($this->nome . "_$this->siglaSezione" . "_prefs", $cookieVal, mktime(0,0,0, date('n'), date('j'), date('Y') + 10), "$this->httpwd/$this->siglaSezione", $this->dominio);
			
		$this->ritorna();
		}
		
	
	
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new frm_config();

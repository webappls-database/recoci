<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//******************************************************************************
include "backoffice.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class pagina extends backoffice
	{
		

	//*****************************************************************************
	function show()
		{
		$dbconn = $this->dammiConnessioneDB();

		// cerchiamo i points
		$sql = "select stage.*" .
				" from stage" .
				" where not stage.deleted" .
				" and stage.route_id=" . $dbconn->interoSql($_GET["route_id"]) .
				" order by stage.ordinal, stage.id";
		$stages = $this->dammiRigheDB($sql, $dbconn);
		if (!$stages->righe)
			$this->mostraMessaggio ("Nessun record", "Nessun record", false, false);
				
		$this->faccelaVedere($stages);
		}

	//*****************************************************************************
	function faccelaVedere(waRigheDB $stages)
		{
		header("Content-Type: text/html; charset=utf-8");			
		
		?>
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="utf-8">
				<style type="text/css">
					html, body, #map-canvas { height: 100%; margin: 30px; padding: 0;}
				</style>

				<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBYqQVl6l_G4x6l7LvGZn0VONj51fVfHLE"></script>
				<script type="text/javascript" src="../ui/js/backoffice/show_map.js"></script>
				<script type="text/javascript">
					var markers = [<?php

										$virgola = "";
										foreach ($stages->righe as $stage)
											{
											echo "$virgola{id: $stage->id, route_id: $stage->route_id, lat: $stage->latitude, lng:$stage->longitude}";
											$virgola = ",";
											}

									?>];
					google.maps.event.addDomListener(window, 'load', function() {showMap("map-canvas", markers, true)});
				</script>

			</head>
			<body>
				<div id="map-canvas">
				</div>
			</body>
		</html>



		<?php
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
$pagina = new pagina();
$pagina->show();
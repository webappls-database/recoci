<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
include "backoffice.inc.php";

//*****************************************************************************
class frm_login extends backoffice
	{
	/**
	 *
	 * @var waModulo
	 */
	var $modulo;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		
		$this->creaModulo();

		if ($this->modulo->daAggiornare())
			{
			$this->eseguiLogin();
			}
		else
			{
			$this->mostraPagina();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il modulo e la manda in output
	* @return void
	*/
	function mostraPagina()
		{
		$this->aggiungiElemento("$this->titolo Login", "titolo");
		$this->aggiungiElemento($this->modulo);
		$this->mostra();
			
		}
		
	//***************************************************************************
	function creaModulo()
		{
		$this->modulo = $this->dammiModulo();
		$ctrl = $this->modulo->aggiungiEmail("email", "E-mail", false, true);

		$ctrl = $this->modulo->aggiungiPassword("pwd", "Password", false, true);
		
		$this->modulo->aggiungiNonControllo("separatore_sopra_bottoniera");
		$button = new waBottone($this->modulo, 'cmd_invia', 'Accedi');
		$this->modulo->aggiungiNonControllo("separatore_sotto_bottoniera");
		
		$this->modulo->leggiValoriIngresso();
		}
	
	//***************************************************************************
	function eseguiLogin()
		{
		$this->verificaObbligo($this->modulo);
		
		$dbconn = $this->dammiConnessioneDB();
		$sql = "select user.*," .
				" station.name as station_name," .
				" station.id_city" .
				" from user" .
				" join station on user.id_station=station.id" .
				" where user.email=" . $dbconn->stringaSql($this->modulo->email) .
//				" and users.supervisor" .
				" and not user.deleted";
		$record = $this->dammiRigheDB($sql, $dbconn, 1)->righe[0];
		if (!$record)
			{
			$this->mostraMessaggio("Accesso non abilitato", "Accesso non abilitato");
			}

		if ($this->decryptPassword($record->pwd) != $this->modulo->pwd)
			{
			$this->mostraMessaggio("Accesso non abilitato", "Accesso non abilitato");
			}

		$this->utente = $this->record2Array($record);
		$this->loggaAccesso($dbconn);
		
		$this->ridireziona($this->paginaIniziale);
		}
			    
//*****************************************************************************
	}
	
//*****************************************************************************
// istanzia la pagina
$page = new frm_login();

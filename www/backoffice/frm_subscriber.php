<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//*****************************************************************************
include "backoffice.inc.php";

//*****************************************************************************
class frm_subscriber extends backoffice
	{
	/**
	 *
	 * @var waModulo
	 */
	var $modulo;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		
		$this->creaModulo();

		if ($this->modulo->daAggiornare())
			{
			$this->aggiornaRecord();
			}
		elseif ($this->modulo->daEliminare())
			{
			$this->eliminaRecord($this->modulo->record);
			}
		else
			{
			$this->mostraPagina();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il modulo e la manda in output
	* @return void
	*/
	function mostraPagina()
		{
		$contesto = $this->dammiContestoTitolo("station");
		$this->aggiungiElemento("Scheda iscrizione $contesto", "titolo");
		$this->aggiungiElemento($this->modulo);
		$this->mostra();
			
		}
		
	//***************************************************************************
	function creaModulo()
		{
		$this->modulo = $this->dammiModulo();
		$this->modulo->righeDB = $this->dammiRecordset();
		$riga = $this->modulo->righeDB->righe[0];
		$solaLettura = false;

		$this->modulo->aggiungiTesto("surname", "Cognome", $solaLettura, true);
		$this->modulo->aggiungiTesto("name", "Nome", $solaLettura, true);
		$ctrl = $this->modulo->aggiungiOpzione("sex", "Sesso", $solaLettura, true);
			$ctrl->lista = array("F" => "F", "M" => "M");
			
		if ($this->preferenzeUtente["data_inline"])
			$this->modulo->aggiungiGenerico("waData_inline", "birth_date", "Data di nascita (ggmmaaaa)", $solaLettura, true);
		else
			$this->modulo->aggiungiData("birth_date", "Data di nascita", $solaLettura, true)->annoPartenza = 1901;
			
		if ($this->preferenzeUtente["selezione_ext"])
			$this->modulo_dammiSelezioneExt($this->modulo, "id_city_birth", "Luogo di nascita", $solaLettura, true, true, "city", 'id', "concat (city_name, ' (', city_province_code, ')')");
		else
			$this->modulo->aggiungiSelezione("id_city_birth", "Luogo di nascita", $solaLettura, true);
				
		$this->modulo->aggiungiTesto("tax_code", "Codice fiscale (calcolato)", true);
		$this->modulo->aggiungiLogico("forced", "Forzatura", $solaLettura);
		
		$this->modulo->aggiungiTesto("address", "Indirizzo di residenza", $solaLettura);
		if ($this->preferenzeUtente["selezione_ext"])
			$ctrl = $this->modulo_dammiSelezioneExt($this->modulo, "id_city_residential", "Luogo di residenza", $solaLettura, false, true, "city", 'id', "concat (city_name, ' (', city_province_code, ')')");
		else
			$ctrl = $this->modulo->aggiungiSelezione("id_city_residential", "Luogo di residenza", $solaLettura);
		// per default la citta di residenza è quella del seggio
		$ctrl->valore = $this->utente["id_city"];
		
		$this->modulo->aggiungiEmail("email", "E-Mail", $solaLettura);
		$this->modulo->aggiungiTesto("phone", "Telefono", $solaLettura);
		
//		$ctrl = $this->modulo->aggiungiCaricaFile("doc", "Documento", $solaLettura);
//			$this->setUrlDoc($ctrl);

		$this->modulo->aggiungiAreaTesto("notes", "Note", $solaLettura);

		$cmdOkCaption = $riga ? 'Registra' : "Registra e continua";
		$elimina = $riga ? true : false;
		$separatore = $riga ? true : false;
		$nextButtonLeft = $this->modulo_bottoniSubmit($this->modulo, 
														$solaLettura, 
														$elimina, 
														$cmdOkCaption,
														$cmdCancelCaption = 'Annulla', 
														$cmdDeleteCaption = 'Elimina',
														$cmdCloseCaption = 'Chiudi',
														$buttonWidth = 120,
														$separatore);
		if (!$solaLettura && !$riga)
			{
			$okCtrl = new waBottone($this->modulo, 'cmd_invia_chiudi', "Registra e chiudi");
			$okCtrl->sinistra = $nextButtonLeft;
			$okCtrl->larghezza = 120;
			$this->modulo->aggiungiNonControllo("separatore_sotto_bottoniera");
			}
		
		$this->modulo->leggiValoriIngresso();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waRigheDB
	*/
	function dammiRecordset()
		{
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT *" .
				" FROM subscriber" .
				" WHERE id=" . $dbconn->interoSql($_GET['id']) . 
				" AND NOT deleted";
			
		$righeDB = $this->dammiRigheDB($sql, $dbconn, $_GET['id'] ? 1 : 0);
		$riga = $righeDB->righe[0];
				
		if ($_GET['id'])
			{
			if (!$riga)
				$this->mostraMessaggio("Record non trovato", "Record non trovato", false, true);
			if (!$this->utenteSupervisore())
				$this->mostraMessaggio("Accesso non abilitato", "Accesso non abilitato", false, true);
			}
			
		return $righeDB;
		}
		
	//***************************************************************************
	function rpc_verificaCF($name, $surname, $sex, $birth_date, $id_city_birth)
		{
		$retval["esito"] = "errore";
		$retval["descrizione_errore"] = "impossibile calcolare i codice fiscale";
		$retval["cf"] = "";
		
		$retval["cf"] = $this->dammiCodiceFiscale($name, $surname, $sex, $birth_date, $id_city_birth);
		if (!$retval["cf"])
			return $retval;
		
		$riga = $this->esisteCF($retval["cf"]);
		if ($riga)
			{
			$retval["descrizione_errore"] = "Il cittadino risulta già registrato " .
												" al seggio $riga->station_name" .
												" alle ore " . date("H:i:s", $riga->creation_time) .
												" del " . date("d/m/Y", $riga->creation_time) .
												" da parte di $riga->user_name .";
			return $retval;
			}
			
		$retval["esito"] = "ok";
		$retval["descrizione_errore"] = "";
		
		return $retval;
		}
		
	//***************************************************************************
	function aggiornaRecord()
		{
		// controlli obbligatorieta' e formali
		$this->verificaObbligo($this->modulo);
		
		// data di nascita: devono avere compiuto 18 anni
		if ($this->modulo->birth_date > mktime(0, 0, 0, date("n"), date("j"), date("Y") - 18))
			$this->mostraMessaggio("Data di nascita non valida", "La data di nascita è relativa a un minorenne");
		
		$riga = $this->modulo->righeDB->righe[0];
		if (!$riga)
			{
			$riga = $this->modulo->righeDB->aggiungi();
			$riga->creation_time = time();
			$riga->id_user = $this->utente["id"];
			$riga->id_station = $this->utente["id_station"];
			}
		else 
			{
			$this->verificaViolazioneLock($this->modulo);
			}
			
			
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$dbconn->iniziaTransazione();
		
		$this->modulo->salva();
		
		// calcola e verifica il codice fiscale
		$riga->tax_code = $this->dammiCodiceFiscale($riga->name, $riga->surname, $riga->sex, $riga->birth_date, $riga->id_city_birth);
		if (!$riga->forced)
			{
			if (!$riga->tax_code)
				$this->mostraMessaggio ("Errore riconoscimento", "Si è verificato un errore nel riconoscimento dell'iscritto: ricontrolla i dati.");
			if ($this->esisteCF($riga->tax_code))
				$this->mostraMessaggio ("Errore riconoscimento", "L'iscritto è già censito: ricontrolla i dati.");
			}
			
		$this->setEditorData($riga);
		$this->salvaRigheDB($riga->righeDB);
		$idInserito = $dbconn->ultimoIdInserito();
//		$this->salvaDoc($this->modulo->controlliInput["doc"]);
		$dbconn->confermaTransazione();

		$valoriRitorno = $idInserito ? array_merge(array("idInserito" => $idInserito), $this->modulo->input) : $this->modulo->input;
		
		// fuori standard: in inserimento rimaniamo nella scheda,salvo che non
		// chiedano esplicitamente di uscire
		if ($_GET["id"] || $this->modulo->cmd_invia_chiudi)
			$this->ritorna($valoriRitorno);
		else
			{
			$url = "?id_station=$_GET[id_station]" .
					"&" . WAMODULO_CHIAVE_OPERAZIONE . "=" . WAMODULO_OPE_INSERIMENTO .
					"&esito_inserimento=ok";
			$this->ridireziona ($url);
			}
		}
		
	//***************************************************************************
	function dammiCodiceFiscale($name, $surname, $sex, $birth_date, $id_city_birth)
		{
		
		$vocali=array("A","E","I","O","U");
		$alfabetoMesi = array( 'A', 'B', 'C', 'D', 'E',
							   'H', 'L', 'M', 'P', 'R',
							   'S', 'T');

		$alfabeto = array( 'A', 'B', 'C', 'D', 'E',
				   'F', 'G', 'H', 'I', 'J',
							'K', 'L', 'M', 'N', 'O',
				   'P', 'Q', 'R', 'S', 'T',
				   'U', 'V', 'W', 'X', 'Y', 'Z');

		// Caratteri posizione dispari
		$PD['0'] = 1; $PD['B'] = 0; $PD['M'] = 18; $PD['X'] = 25;
		$PD['1'] = 0; $PD['C'] = 5; $PD['N'] = 20; $PD['Y'] = 24;
		$PD['2'] = 5; $PD['D'] = 7; $PD['O'] = 11; $PD['Z'] = 23;
		$PD['3'] = 7; $PD['E'] = 9; $PD['P'] = 3;
		$PD['4'] = 9; $PD['F'] = 13; $PD['Q'] = 6;
		$PD['5'] = 13; $PD['G'] = 15; $PD['R'] = 8;
		$PD['6'] = 15; $PD['H'] = 17; $PD['S'] = 12;
		$PD['7'] = 17; $PD['I'] = 19; $PD['T'] = 14;
		$PD['8'] = 19; $PD['J'] = 21; $PD['U'] = 16;
		$PD['9'] = 21; $PD['K'] = 2; $PD['V'] = 10;
		$PD['A'] = 1; $PD['L'] = 4; $PD['W'] = 22;

		// Mesi
		$mese[0] = "Gennaio";
		$mese[1] = "Febbraio";
		$mese[2] = "Marzo";
		$mese[3] = "Aprile";
		$mese[4] = "Maggio";
		$mese[5] = "Giugno";
		$mese[6] = "Luglio";
		$mese[7] = "Agosto";
		$mese[8] = "Settembre";
		$mese[9] = "Ottobre";
		$mese[10] = "Novembre";
		$mese[11] = "Dicembre";

		$gender[0] = "Maschile";
		$gender[1] = "Femminile";

		
		//prendi variabili e converti
		$cognome = $surname;
		$nome = $name;
		//$data = $_GET['data'];
		$gender = $sex == "M" ? "Maschile" : "Femminile";

		$DG = date("j", $birth_date);
		$DM = $mese[date("n", $birth_date) - 1];
		$DA = date("Y", $birth_date);
		
		// CODICE COMUNE
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$query = "SELECT city_code_land FROM city WHERE id=" . $dbconn->interoSql($id_city_birth);
		$riga = $this->dammiRigheDB($query, $dbconn, 1)->righe[0];
		if (!$riga)
			return "";
		$codice_istat_comune = $riga->city_code_land;
		
		// Il codice fiscale � formato da 7 parti
		$parte[0] = "";
		$parte[1] = "";
		$parte[2] = "";
		$parte[3] = "";
		$parte[4] = "";
		$parte[5] = "";
		$parte[6] = "";
		// CODICE PER IL COGNOME (consonanti n�1-2-3 + eventuali vocali)
		$cognome = strtoupper($cognome);
		$nvocali = preg_match_all('/[AEIOU]/i',$cognome,$matches1);
		$nconsonanti = preg_match_all('/[BCDFGHJKLMNPQRSTVWZXYZ]/i',$cognome,$matches2);
		if($nconsonanti>=3)  $parte[0] = $matches2[0][0] . $matches2[0][1] . $matches2[0][2];
		else
		{

			  for($i = 0; $i < $nconsonanti; $i++)
			  {
					$parte[0] = $parte[0] . $matches2[0][$i];
			  }
			  $n = 3-strlen($parte[0]);
			  for($i = 0; $i < $n; $i++)
			  {
					$parte[0] = $parte[0] . $matches1[0][$i];
			  }
			  $n = 3-strlen($parte[0]);
			  for($i = 0; $i < $n; $i++)  $parte[0] = $parte[0] . "X";
		}

		// CODICE PER IL NOME (consonanti n�1-3-4, oppure 1-2-3 se sono 3; se sono meno di 3: vocali)
		$nome = strtoupper($nome);
		$nvocali = preg_match_all('/[AEIOU]/i',$nome,$matches1);
		$nconsonanti = preg_match_all('/[BCDFGHJKLMNPQRSTVWZXYZ]/i',$nome,$matches2);
		if($nconsonanti>=4) $parte[1] = $matches2[0][0] . $matches2[0][2] . $matches2[0][3];
		else if($nconsonanti==3)  $parte[1] = $matches2[0][0] . $matches2[0][1] . $matches2[0][2];
		else
		{
			  for($i = 0; $i < $nconsonanti; $i++)
			  {
					$parte[1] = $parte[1] . $matches2[0][$i];
			  }
			  $n = 3-strlen($parte[1]);
			  for($i = 0; $i < $n; $i++)
			  {
					$parte[1] = $parte[1] . $matches1[0][$i];
			  }
			  $n = 3-strlen($parte[1]);
			  for($i = 0; $i < $n; $i++)  $parte[1] = $parte[1] . "X";
		}

		// CODICE ANNO (Ultime 2 cifre dell'anno)
		$arrAnno = str_split($DA);
		$parte[2] = $arrAnno[2] . $arrAnno[3];

		// CODICE MESE (A = Gennaio, B = Febbraio, ecc.)
		$m = array_search($DM, $mese);
		$parte[3] = $alfabetoMesi[$m];

		// CODICE GIORNO (se uomo; n. giorno (DD); altrimenti n. giorno + 40)
		if($gender == "Maschile") $parte[4] = $DG;
		else $parte[4] = $DG +40;
		if(strlen($parte[4]) == 1) $parte[4] = "0" . $parte[4];

		// CODICE COMUNE
		$parte[5] = $codice_istat_comune;

		// CODICE DI CONTROLLO ((caratteri posiz pari + posiz dispari) / 26 -> Carattere di controllo)
		$arrCOD = $parte[0] . $parte[1] . $parte[2] . $parte[3] . $parte[4] . $parte[5];
		$arrCOD = str_split($arrCOD);
		$index = count($arrCOD);
		/* posizione pari */
		$somma1 = 0;
		for($i = 0; $i < 15; $i++)
			if(($i+1)%2==0)
				{
					  if(!in_array($arrCOD[$i], $alfabeto)) $somma1 += $arrCOD[$i];
					  else
					  {
						   $n = array_search($arrCOD[$i], $alfabeto);
						   $somma1 += $n;
					  }
			   }
		/* posizione dispari */
		$somma2 = 0;
		for($i = 0; $i < 15; $i++)
			if(($i+1)%2!=0)
				{
					$somma2 += $PD["$arrCOD[$i]"];
				}
		$somma = $somma1+$somma2;
		$parte[6] = ($somma % 26);
		$parte[6] = $alfabeto[$parte[6]];
		// OUTPUT
		return "$parte[0]$parte[1]$parte[2]$parte[3]$parte[4]$parte[5]$parte[6]";



		}
	
		//***************************************************************************
	function esisteCF($cf)
		{
		$riga = $this->modulo->righeDB->righe[0];
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$sql = "SELECT subscriber.*," .
			" station.name as station_name," .
			" concat(user.name, ' ', user.surname) as user_name," .
			" IF(subscriber.forced, 'si', 'no') AS s_forced" .
			" FROM subscriber" .
			" join station on subscriber.id_station=station.id" .
			" join user on subscriber.id_user=user.id" .
			" where not subscriber.deleted" .
			($riga->id ? " and subscriber.id!=" . $dbconn->interoSql($riga->id) : "") .
			" and not subscriber.forced" .
			" and tax_code=" . $dbconn->stringaSql($cf);
		$riga = $this->dammiRigheDB($sql, $dbconn, 1)->righe[0];

		return $riga;
		}
		
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	//***************************************************************************
	/**
	 * NON USATA!!!!!
	 * @param type $valori_form
	 * @return type
	 */
	function rpc_ricarica_opzioni_id_city_birth($valori_form)
		{
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$vf = @json_decode($valori_form, true);
		$sql = "SELECT id, concat (city_name, ' (', city_province_code, ')') as city_name" .
				" FROM city" .
				" WHERE city_name LIKE " . $dbconn->stringaSql("%" . $vf["id_city_birth_testo"] . "%") .
				" AND NOT deleted" .
				" ORDER BY city_name";
		
		return $this->modulo->controlliInput["id_city_birth"]->ricaricaOpzioniSelezioneExt($sql);
		}
		
	//***************************************************************************
	/**
	 * NON USATA!!!!!
	 * @param type $valori_form
	 * @return type
	 */
	function rpc_ricarica_opzioni_id_city_residential($valori_form)
		{
		$dbconn = $this->modulo->righeDB->connessioneDB;
		$vf = @json_decode($valori_form, true);
		$sql = "SELECT id, concat (city_name, ' (', city_province_code, ')') as city_name" .
				" FROM city" .
				" WHERE city_name LIKE " . $dbconn->stringaSql("%" . $vf["id_city_residential_testo"] . "%") .
				" and city_province_code=" . $dbconn->stringaSql("BO") .
				" AND NOT deleted" .
				" ORDER BY city_name";
		
		return $this->modulo->controlliInput["id_city_residential"]->ricaricaOpzioniSelezioneExt($sql);
		}
		
//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new frm_subscriber();

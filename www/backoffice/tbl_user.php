<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
//******************************************************************************
include "backoffice.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class tbl_user extends backoffice
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();
		$this->finestraFiglia = (bool) $_GET["id_station"];

		$this->aggiungiElemento($this->dammiMenu());
		$contesto = $this->dammiContestoTitolo("station");
		$this->aggiungiElemento("Elenco operatori $contesto", "titolo");
		$this->aggiungiElemento($this->dammiTabella());
		$this->mostra();
		}

	//*****************************************************************************
	/**
	 * @return waTabella
	 */
	function dammiTabella()
		{
		// creazione della tabella
		$dbconn = $this->dammiConnessioneDB();
		$sql = "SELECT user.*," .
			" station.name as station_name," .
			" IF(user.supervisor, 'si', 'no') AS s_supervisor" .
			" FROM user" .
			" join station on user.id_station=station.id" .
			" WHERE NOT user.deleted" .
			($_GET["id_station"] ? " and user.id_station=" . $dbconn->interoSql($_GET["id_station"]) : '') .
			" ORDER BY station_name, user.name";
		
		$tabella = parent::dammiTabella($sql);
		$tabella->paginaModulo = "frm_user.php?id_station=$_GET[id_station]";
		
		if (!$this->utenteSupervisore())
			{
			$tabella->eliminaAzione ("Nuovo");
			$tabella->azioni["Modifica"]->funzioneAbilitazione = array($this, "recordModificabile");
			$tabella->azioni["Elimina"]->funzioneAbilitazione = array($this, "recordModificabile");
			}
				
		$tabella->aggiungiColonna("id", "ID", false, false, false)->aliasDi = "user.id";
		$tabella->aggiungiColonna("surname", "Cognome")->aliasDi = "user.surname";
		$tabella->aggiungiColonna("name", "Nome")->aliasDi = "user.name";
		$col = $tabella->aggiungiColonna("email", "E-mail");
			$col->aliasDi = "user.email";
			$col->link = true;
		$tabella->aggiungiColonna("phone", "Telefono")->aliasDi = "user.phone";
		$col = $tabella->aggiungiColonna("s_supervisor", "Supervisore", true, true, true, WATBL_ALLINEA_CENTRO);
			$col->aliasDi = "IF(user.supervisor, 'si', 'no')";

		if (!$_GET["id_station"])
			{
			$tabella->aggiungiColonna("station_name", "Seggio")->aliasDi = "station.name";
			}
			
		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$tabella->caricaRighe()) $this->mostraErroreDB($tabella->righeDB->connessioneDB);

		return $tabella;
		}

	//*****************************************************************************
	function recordModificabile(waTabella $tabella)
		{
		return $this->utenteSupervisore();
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new tbl_user();
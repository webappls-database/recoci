<?php
/**
* @package ReCoCI - Registro Consultazioni Civiche
* @version 0.1
* @author G.Gaiba, F.Monti
* @copyright (c) 2016 {@link http://www.webappls.com WebAppls} Bologna, Italy
* @copyright (c) 2016 {@link http://www.database.it Database Informatica} Bologna, Italy
* @license http://www.gnu.org/licenses/gpl.html GPLv3
*/
if (!defined('__VERSIONCONFIG_VARS'))
{
	define('__VERSIONCONFIG_VARS',1);
	
	define('APPL_REL', 							'0.1.10');
	define('APPL_REL_DATE', 					mktime(0,0,0, 3, 12, 2016));

	
} //  if (!defined('__VERSIONCONFIG_VARS'))
